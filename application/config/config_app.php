<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Configuraciones de Template
|--------------------------------------------------------------------------
*/

/*
| Nombre del sistema WEB
*/
$config['app_name'] = 'izimovies';

/*
| Establece el ID del rol Administrador del sistema
*/
$config['idRolAdmin'] = '2a2e9a58102784ca18e2605a4e727b5f';

$config['idAdminPrincipal'] = '2a2e9a58102784ca18e2605a4e727b5f';

/*
| Plugins o librerias a utilizar en cada módulo
*/

$config['plugins']  = [
    'generales' =>
        [
            'css' =>
                [
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/animate.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/style.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/spinner.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/styleHelper.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/styleFixes.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/select2.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/icons.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/css/bootstrap.min.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/plugins/font-awesome/css/all.min.css'>",
                    "<link rel='stylesheet' href='".$this->base_url()."assets/plugins/fullcalendar/css/fullcalendar.min.css'>",
                ],
            'js' =>
                [
                    //JQuery
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/jquery.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/popper.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/bootstrap.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/modernizr.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/waves.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/jquery.nicescroll.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/jquery.scrollTo.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/jquery.slimscroll.js'></script>",
                    
                    //JQuery UI
                    "<script type='text/javascript' src='".$this->base_url()."assets/plugins/jquery-ui/jquery-ui.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/plugins/moment/moment.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/plugins/fullcalendar/js/fullcalendar.min.js'></script>",
                    "<script type='text/javascript' src='".$this->base_url()."assets/pages/calendar-init.js'></script>",
                    
                    //AppJS
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/app.js'></script>",
                    
                    //Plugins
                    "<script src='https://cdn.jsdelivr.net/npm/sweetalert2@8'></script>",
                    //Init
                    "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init.js'></script>"
                ]
        ],
    'modulos' =>
        [
            'usuarios' =>
                [
                    'css' =>
                        [
                        ],
                    'js' =>
                        [
                            "<script type='text/javascript' src='".$this->base_url()."assets/plugins/select2/js/select2.min.js'></script>",
                            "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init_usuarios.js'></script>"
                        ]
                ],
            'roles' =>
                [
                    'css' =>
                        [
                        ],
                    'js' =>
                        [
                            "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init_roles.js'></script>"
                        ]
                ],
            'inicio' =>
                [
                    'css' =>
                        [
                        ],
                    'js' =>
                        []
                ],
                
            'reportes' =>
                [
                    'css' =>
                        [
                            "<link rel='stylesheet' href='".$this->base_url()."assets/css/styleReportes.css'>",
                        ],
                    'js' =>
                        [
                            "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init_reportes.js'></script>",
                            "<script type='text/javascript' src='".$this->base_url()."assets/js/bootgrid/jquery.bootgrid.js'></script>",
                        ]
                ],
                'categorias' =>
                    [
                        'css' =>
                            [
                            ],
                        'js' =>
                            [
                                "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init_categorias.js'></script>"
                            ]
                    ],
                'registrarpeliculas' =>
                    [
                        'css' =>
                            [
                            ],
                        'js' =>
                            [
                            "<script type='text/javascript' src='".$this->base_url()."assets/plugins/select2/js/select2.min.js'></script>",
                            "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init_registrarPeliculas.js'></script>"
                            ]
                    ],
                'peliculas' =>
                    [
                        'css' =>
                            [
                            "<link rel='stylesheet' href='".$this->base_url()."assets/css/stylePeliculas.css'>",
                            ],
                        'js' =>
                            [
                            "<script type='text/javascript' src='".$this->base_url()."assets/plugins/select2/js/select2.min.js'></script>",
                            "<script type='text/javascript' src='".$this->base_url()."assets/js/app/init_peliculas.js'></script>"
                            ]
                    ],
                
        ]
];

/*
| Parámetros para la traducción de una fecha
*/
$config['date_es'] = [
    'Monday'    => 'Lunes',
    'Tuesday'   => 'Martes',
    'Wednesday' => 'Miércoles',
    'Thursday'  => 'Jueves',
    'Friday'    => 'Viernes',
    'Saturday'  => 'Sábado',
    'Sunday'    => 'Domingo',
    'Mon'       => 'Lun',
    'Tue'       => 'Mar',
    'Wed'       => 'Mie',
    'Thu'       => 'Jue',
    'Fri'       => 'Vie',
    'Sat'       => 'Sa',
    'Sun'       => 'Do',
    'January'   => 'Enero',
    'February'  => 'Febrero',
    'March'     => 'Marzo',
    'April'     => 'Abril',
    'May'       => 'Mayo',
    'June'      => 'Junio',
    'July'      => 'Julio',
    'August'    => 'Agosto',
    'September' => 'Septiembre',
    'October'   => 'Octubre',
    'November'  => 'Noviembre',
    'December'  => 'Diciembre',
];

/*
| Configuración de los parámetros para la paginación en las vistas index
*/
$config['pagination'] = [
    'use_page_numbers'   => TRUE,
    'page_query_string'  => false,
    'reuse_query_string' => true,
    'per_page'           => 15,
    'num_links'          => 3,
    'uri_segment'        => 3];

/*
| Configuración de los parámetros para el envio de emails
*/
$config['email'] = [
    'protocol'  => 'smtp',
    'smtp_host' => 'mail.geniuscode.mx',
    'smtp_user' => 'noreply@geniuscode.mx',
    'smtp_pass' => 'Gcode.18',
    'smtp_port' => '587',
    'charset'   => 'utf-8',
    'wordwrap'  => true,
    'validate'  => true,
    'mailtype'  => 'html'
];