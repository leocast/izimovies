<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['full_tag_open'] = '<ul class="pagination pagination-sm ">';
$config['full_tag_close'] = '</ul>';

$config['first_link'] = 'Primera';
$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';

$config['last_link'] = 'Última';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';

$config['next_link'] = 'Siguiente';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = 'Anterior';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';

$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
$config['cur_tag_close'] = '</a></li>';

$config['attributes'] = array('class' => 'page-link');
$config['attributes']['rel'] = FALSE;
$config['display_pages'] = TRUE;