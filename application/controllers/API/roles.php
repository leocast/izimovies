<?php 
use Firebase\JWT\JWT;
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__) . '/..') .'/process/roles.php');

class Roles extends RolesProcess
{
    
    private $sistemaApi;
    
    public function __construct() {
        parent::__construct();

        $this->sistemaApi['modulo']['nombre'] = 'roles';
        $this->sistemaApi['modulo']['id']     = '4295e8065f2f640103f566df3329e17f';
        $this->sistemaApi['usuario']          = $this->_apiConfig(['methods' => ['POST'], 'requireAuthorization' => true,])['token_data'];
    }

    public function getRoles() {
     
        initApi();
        
        validarPermiso('acceso', true);
        
        actualizarConcurrencia("ACCE", $this->sistemaApi, $this->sistemaApi['usuario']['id'], null, true);

        $post                = (object)$this->input->post();
        $post->permisosRoles = cargarPermisos($this->sistemaApi, true);
        
        $result = $this->procesarIndex($post, $this->sistemaApi);
        $result = $result->roles;

        //Respuesta
        $helper         = new Helper();
        $helper->estado = true;
        $helper->result = $result;
        
        returnApi($helper);
    }

    public function getDatosNuevo(){
        
        initApi();

        validarPermiso('crear', true);
       
        actualizarConcurrencia("CREA", $this->sistemaApi, $this->sistemaApi['usuario']['id'], null, true);

        $result = $this->procesarNuevo(true, $this->sistemaApi);
        
        unset($result->active);
        unset($result->actives_checks);

        //Respuesta
        $helper         = new Helper();
        $helper->estado = true;
        $helper->result = $result;
      
        returnApi($helper);
        
    }

    public function nuevo(){
        
        initApi();

        $post = $this->input->post();
        $post = $this->formatearPostNuevo($post);

        $helper = $this->registrarNuevoProces($post);

        returnApi($helper, true);
   
    }

    public function getRol(){
        
        initApi();
        
        validarPermiso('modificar', true);

        $rol = $this->getRol($idRol);

        verificarExistencia($rol, false, "El rol no existe");
        
        actualizarConcurrencia("MODI", $this->sistemaApi, $this->sistemaApi->usuario['id'], null, true);

        $post = (object)$this->input->post();
        
        $helper = $this->procesarEditar($post->id, true, $this->sistemaApi);

        if ($helper->estado) 
            $helper = $this->formatearGetRol($helper);

        returnApi($helper);
    }

    public function editar(){
        
        initApi();
        
        $post = $this->input->post();
        $post = $this->formatearPostNuevo($post);

        $helper = $this->registrarEditarProces($post->id, $post);;

        returnApi($helper, true);
        
    }

    public function eliminar(){
             
        initApi();
        
          
        validarPermiso('eliminar', true);

        $rol = $this->getRol($idRol);

        verificarExistencia($rol, false, "El rol no existe");
        
        actualizarConcurrencia("ELIM", $this->sistemaApi, $this->sistemaApi->usuario['id'], null, true);

        $post = (object)$this->input->post();

        $helper = $this->eliminarProces($post->id, true, $this->sistemaApi);

        returnApi($helper);
            
    }

    //Funciones privadas
    private function formatearPostNuevo($post){
        
        $post['acciones'] = json_decode($post['acciones'], true);

        foreach ($post['acciones'] as $i => $accion) {
            $post['accion_ident-'.$accion] = "on";
            unset($post['acciones'][$i]);
        }
        
        unset($post['acciones']);

        return (object)$post;

    }

    private function formatearGetRol($helper){
        
        unset($helper->esDisponible);
        unset($helper->modulos);
        unset($helper->acciones);

        if ($helper->active == "checked")
            $helper->activo = true;
        else
            $helper->activo = false;

        unset($helper->active);

        $acciones = [];
        
        foreach ($helper->actives_checks as $i => $check) {
            array_push($acciones, $i);
        }

        unset($helper->actives_checks);
        
        $helper->acciones = $acciones;

        return $helper;
    }
}