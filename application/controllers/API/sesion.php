<?php 
use Firebase\JWT\JWT;
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__) . '/..') .'/process/sesion.php');

class Sesion extends SesionProcess
{
    public function __construct() {
        parent::__construct();

        //Carga de modelos
        $this->load->model('model_usuarios','usuarios');
        $this->load->model('model_permisos','permisos');

        // Carga de helpers
        $this->load->helper('api_helper', 'api');
    }

    public function login() {
        header("Access-Control-Allow-Origin: *");
        // Configuración del API para esta función
        $this->_apiConfig([
            'methods' => ['POST']
        ]);

        // Se obtienen los datos del metodo POST y se valida si no estan vacios
        $post = (object)$this->input->post();
        
        if(!empty($post) and isset($post->usuario) and isset($post->contrasena)){
            
            // Se obtiene el usuario de la base de datos
            $usuario = $this->usuarios->login(strtolower($post->usuario), md5($post->contrasena));
                       
            // Se valida si se encontro el usuario en base a los parametros enviados
            if(!empty($usuario)){
                //Parsear permisos
                $permisos = [];
                $acciones = $this->permisos->getAccionesConPermisosPorRol($usuario->idrol, true);

                if (!empty($acciones)){
                    foreach ($acciones AS $action){
                        $key = array_search($action['nombre'], array_column($permisos, 'alias'));
                        $action['permiso'] = ($action['permiso'] == 't') ? true : false;
                        $action['accion'] = (strtolower(str_replace(' ', '_', $action['accion'])));

                        if ($key === false){
                            $permisos[] = ['alias' => $action['nombre'], 'acciones' => [$action['accion'] => $action['permiso']]];
                        }else{
                            $permisos[$key]['acciones'][$action['accion']] = $action['permiso'];
                        }
                    }
                }

                
                $tokenAutenticacion = nuevoToken();
                $payload = [
                    'exp'                 =>  time() + (60 * 60) * 24 * 7,
                    'id'                  => (!empty($usuario->id)) ? $usuario->id : '',
                    'nombre'              => (!empty($usuario->nombre)) ? $usuario->nombre : '',
                    'rol'                 => (!empty($usuario->rol)) ? $usuario->rol : '',
                    'imagen'              => (!empty($usuario->imagen)) ? $usuario->imagen : '',
                    'token_autenticacion' => $tokenAutenticacion,
                    'permisos'            => $permisos
                ];
                
                // Load Authorization Library or Load in autoload config file
                $this->load->library('authorization_token');
                
                // Generar token API
                $token_api = $this->authorization_token->generateToken($payload);
                
                // Actualizamos en el usuario el token del API y el token (clave) de autenticación
                $this->usuarios->actualizar($usuario->id,
                    [
                        'tokenapi' => $token_api,
                        'tokenaut' => $tokenAutenticacion,
                        'plataforma' => 2,
                        'ultimoacc' => date('Y-m-d H:i:s'),
                        'idultimomod' => null,
                        'idultimaacc' => null,
                        'ultimorec' => null
                    ]
                );
                    
                // Respondemos con los datos obtenidos
                $response = [
                    'status' => true,
                    'message' => "",
                    'token' => $token_api
                ];
                $http_code = self::HTTP_OK;
            }else{
                // Respondemos con un error
                $response = [
                    'status' => false,
                    'message' => "Usuario y/o contraseña incorrectas."
                ];
                $http_code = self::HTTP_BAD_REQUEST;
            }
        }else{
            // Respondemos con un error
            $response = [
                'status' => false,
                'message' => "El usuario y/o contraseña están vacios."
            ];
            $http_code = self::HTTP_BAD_REQUEST;
        }
        // Regresamos los valores
        $this->api_return($response, $http_code);
    }

    public function get_token_info(){
        header("Access-Control-Allow-Origin: *");

        // Configuración del API para esta función
        $usuarioApi = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        // Regresa la informacion encriptada en el token
        $this->api_return(
            [
                'status' => true,
                'message' => "",
                'result' => [
                    'usuario' => $usuarioApi['token_data']
                ],
            ],
            200);
    }

    public function is_valid_token(){
        header("Access-Control-Allow-Origin: *");

        // Configuración del API para esta función
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        // Validamos si la variable obtenida del token tiene datos
        if ($user_data){
            $response = [
                'status' => true,
                'message' => "",
                'result' => [
                    'valid' => true,
                ],
            ];
            $http_code = self::HTTP_OK;
        }else{
            $response = [
                'status' => false,
                'message' => "Token invalido."
            ];
            $http_code = self::HTTP_BAD_REQUEST;
        }

        // Regresamos los datos
        $this->api_return($response,$http_code);
    }

    public function test(){
        header("Access-Control-Allow-Origin: *");

        // Configuración del API para esta función
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]); 

        $http_code = self::HTTP_OK; $this->api_return($response = ['debug' => ($user_data)], $http_code); return;
    }
}