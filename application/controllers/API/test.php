<?php 
use Firebase\JWT\JWT;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

class Test extends API_Controller{

    public function __construct() {
        parent::__construct();

        // Carga de helpers
        $this->load->helper('api_helper', 'api');

    }

    public function get_all(){
        header("Access-Control-Allow-Origin: *");

        // Configuración del API para esta función
        $token_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        // Valida que la sesion sea autentica, es decir que existe el usuario y/o no se halla abierto en otro dispositivo
        $valid_session = check_session_api($token_data['token_data']);

        // Se valida el status con la respuesta anterior
        if($valid_session['status']){
      

        }else{
            // Se responde a la peticion segun el error obtenido
            $response = [
                'status' => $valid_session['status'],
                'message' => $valid_session['message']
            ];
            $http_code = self::HTTP_BAD_REQUEST;
        }

        // Regresamos los datos
        $this->api_return($response, $http_code);
    }

    public function test(){
        header("Access-Control-Allow-Origin: *");

        $token_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);


        $http_code = self::HTTP_OK; $this->api_return($response = ['debug' => ("HI")], $http_code); return;
        //   $this->api_return($response, $http_code);
    }
}