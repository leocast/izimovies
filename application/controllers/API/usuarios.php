<?php 
use Firebase\JWT\JWT;
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__) . '/..') .'/process/usuarios.php');

class Usuarios extends UsuariosProcess
{
    
    private $datosApi;
    
    public function __construct() {
        parent::__construct();

        $this->datosApi['sistema']['modulo']['nombre'] = 'usuarios';
        $this->datosApi['sistema']['modulo']['id']     = 'a1c28da1af69ba622c94f7e3bd95814c';
        $this->datosApi['usuario'] = $this->_apiConfig(['methods' => ['POST'], 'requireAuthorization' => true,])['token_data'];

        $this->datosApi = (object)$this->datosApi;
    }

    public function getUsarios() {
     
        initApi();
        
        $post = (object)$this->input->post();
        $result = $this->procesarIndex($post, true, $this->datosApi);
        $result = $result->usuarios;

        //Respuesta
        $helper         = new Helper();
        $helper->estado = true;
        $helper->result = $result;
        
        returnApi($helper);
    }

    public function nuevo(){
        
        initApi();

        $post = (object)$this->input->post();
        $post->activo = $post->activo == "true" ? "on" : "off";

        $helper = $this->registrarNuevoProces($post);

        returnApi($helper, true);
   
    }

    public function getUsuario(){
        
        initApi();

        $post = (object)$this->input->post();
        
        $helper = $this->procesarEditar($post->id, true, $this->datosApi);

        if ($helper->estado) 
            $helper = $this->formatearGetUsuario($helper);

        returnApi($helper);
    }

    public function editar(){
        
        initApi();
        
        $post = (object)$this->input->post();
        $post->activo = $post->activo == "true" ? "on" : "off";

        $helper = $this->registrarEditarProces($post->id, $post);;

        returnApi($helper, true);
        
    }

    public function eliminar(){
             
        initApi();
        
        $post = (object)$this->input->post();

        $helper = $this->eliminarProces($post->id, true, $this->datosApi);

        returnApi($helper);
            
    }

    public function formatearGetUsuario($helper){
        unset($helper->listaRoles);
        unset($helper->esDisponible);
        
        $result = new stdClass();
        
        $result->estado = true;
        $result->result = $helper;

        unset($helper->estado);

        return $result;
    }

}