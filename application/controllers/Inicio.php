<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('process/inicio.php');

class Inicio extends InicioProcess {

	public function index()
	{
		$this->renderIndex();
	}
}
