<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('process/peliculas.php');

class Peliculas extends PeliculasProcess {
    
    public $nombre;
    private $sistema;

    function __construct(){
        parent::__construct();
        $this->nombre = 'peliculas';
        $this->sistema = getSistema();
    }

	public function index()
	{
        verificarPermisoExistencia($this->sistema, "ACCE");

        $get                = (object)$this->input->get();
        $get->segmento      = strlen($this->uri->segment(3)) > 10 ? "" : $this->uri->segment(3); 
        $get->permisosRoles = cargarPermisos($this->sistema);

        $vista = $this->procesarIndex($get, $this->sistema);

        render('peliculas/index', $vista);
    }

    public function ver($id){

        $vista = $this->procesarVer($id);

        render('peliculas/ver', $vista);
    }
 
}