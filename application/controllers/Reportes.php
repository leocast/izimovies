<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('process/reportes.php');

class Reportes extends ReportesProcess {

    public $nombre;
    
    function __construct(){
        parent::__construct();
        $this->nombre = 'reportes';
    }

	public function index()
	{
		render("reportes/index");
    }
    
    public function generar(){
        $post = (object)$this->input->post();

        $helper = $this->renderPorTipo($post);

        render("reportes/$helper->tipo", $helper);
    }
 
}