<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('process/roles.php');

class Roles extends RolesProcess {
    
    public $nombre;
    private $sistema;

    function __construct(){
        parent::__construct();
        $this->sistema = getSistema();
        $this->nombre = 'roles';
    }

	public function index()
	{
        verificarPermisoExistencia($this->sistema, "ACCE");
        
        $get                = (object)$this->input->get();
        $get->segmento      = strlen($this->uri->segment(3)) > 10 ? "" : $this->uri->segment(3); 
        $get->permisosRoles = cargarPermisos($this->sistema);

        $vista = $this->procesarIndex($get, $this->sistema);

        render('roles/index', $vista);
    }
    
    public function nuevo(){

        verificarPermisoExistencia($this->sistema, "CREA");
        
        $vista            = $this->procesarNuevo();
        $vista->urlAccion = 'roles/registrarNuevo';

        render('roles/formulario', $vista); 
    }

    public function registrarNuevo(){
        
        $post   = (object)$this->input->post();
        $helper = $this->registrarNuevoProces($post);

        redireccionar($helper, $this);
   
    }

    public function editar($idRol){

        $esDisponible = verificarPermisoExistencia($this->sistema, "MODI", $this->nombre, $idRol);
        
        if ($esDisponible) {
            $vista            = $this->procesarEditar($idRol);
            $vista->urlAccion = 'roles/registrarEditar/'.$idRol;
  
            render('roles/formulario', $vista); 

        } else {
            redirect('roles');
        }
    }

    public function registrarEditar($idRol){
        
        $post   = (object)$this->input->post();
        $helper = $this->registrarEditarProces($idRol, $post);

        redireccionar($helper, $this, true, $idRol);
     
    }

    public function eliminar($idRol){
        
        $esDisponible = verificarPermisoExistencia($this->sistema, "ELIM", $this->nombre, $idRol);
        
        if ($esDisponible) {
            $helper = $this->eliminarProces($idRol);
            redireccionar($helper, $this);

        } else {
            redirect('roles');
        }
      
    }
 
}
