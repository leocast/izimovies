<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('process/sesion.php');

class Sesion extends SesionProcess {

	private $imagenConfig;

		function __construct(){
			parent::__construct();

			$this->imagenConfig['upload_path']      = './assets/img/perfil/';
			$this->imagenConfig['allowed_types']    = 'jpg|png|jpeg';
			$this->imagenConfig['max_size']         = 1024;
			$this->imagenConfig['overwrite']        = true;
			$this->imagenConfig['file_ext_tolower'] = true;
		}

		public function index() {
			if ($this->session->has_userdata('usuario')) 
				redirect(base_url("inicio"));
			else
				renderLogin('sesion/login');
		}

		public function login(){
			
			if ($this->session->has_userdata('usuario')) {
				
					if($this->verificarSesion()){
						redirect(base_url("inicio"));
					}else {
						session_destroy();
						show_error('Se inició sesión en otro dispositivo','404','Sesión Doble');
					}
			
				} else {
				
				$post = (object)$this->input->post();
			
				if ($this->validarPost($post)){
					if($this->iniciarSesion($post)){
						redirect(base_url("inicio"));
					} else { 
						$vista = new stdClass();
						$vista->mensaje = crearMensaje('danger','fas fa-bell','Ups!','Usuario o contraseña incorrectos');
						renderLogin('sesion/login', $vista);
					}				
				}else{
					renderLogin('sesion/login');
				}
			
			}
		}

		public function cerrarSesion(){
			$this->cerrar();
			session_destroy();
			redirect('sesion');
		}

		public function olvidoContrasenaProces(){
			$post = (object)$this->input->post();

			$helper = $this->enviarEmailRecuperacion($post);
			$vista 	= new stdClass();

			if ($helper->estado) {
				$vista->mensaje = crearMensaje('success','fas fa-bell','Éxito!', $helper->mensaje);
				renderLogin('sesion/login', $vista);
			} else {
				$vista->mensaje = crearMensaje('danger','fas fa-bell','Ups!', $helper->mensaje);
				renderLogin('sesion/olvidoContrasena', $vista);
			}
		}

		public function reestablecerContrasenaC(){
			
			$post 	= (object)$this->input->post();
			$vista 	= new stdClass();

			if (isset($post->tokenRec)){
		
				$helper = $this->reestablecerContrasenaProces($post);
				
				if ($helper->estado) {
					$vista->mensaje = crearMensaje('success','fas fa-check','Éxito!','Contraseña actualizada correctamente');
					renderLogin('sesion/login', $vista);
				} else {
					$vista->tokenRec = $helper->tokenRec;
					renderLogin('sesion/reestablecerContrasena', $vista);
				}

			} else {
				$vista->mensaje = crearMensaje('danger','fas fa-bell','Ups!','Ha ocurrido un error');
				renderLogin('sesion/login', $vista);
			}
		}

		//VISTAS
		public function olvidoContrasena(){
			renderLogin('sesion/olvidoContrasena');
		}

		public function registrarse(){
			renderLogin('sesion/nuevoCliente');
		}

		public function registrarNuevoUsuario(){
			$post = (object)$this->input->post();

			$this->load->model('model_usuarios', 'usuarios');
			$campos =  $this->usuarios->camposUsuarios;
	
			aplicarReglas($campos, $post);
			if($this->form_validation->run()){
	
				$usuario = new stdClass();
	
				$usuario->id         = nuevoId('usuarios');
				$usuario->usuario    = $post->usuario;
				$usuario->contrasena = md5($post->contrasena);
				$usuario->idrol      = "bfa5d96b849eb8c080877e397d9b46d2"; //Cliente
				$usuario->nombre     = $post->nombre;
				$usuario->correo     = $post->correo;
				$usuario->activo     = true;
				$usuario->creacion   = date("Y-m-d H:i:s");
				
				//GESTIONAR IMAGEN
				$imagenSubida = true;
				$this->imagenConfig['file_name']      = $usuario->id;
			   
				$this->load->library('upload', $this->imagenConfig);
			  
				if (!$this->upload->do_upload('imagen')){
					$usuario->imagen = "perfilRegistro.jpg";
					$imagenSubida = false;
				}else{
					$usuario->imagen = $usuario->id.$this->upload->file_ext;
				}
	
				$this->usuarios->agregar($usuario);

				$vista = new stdClass();
				$mensaje = "<input hidden id='nuevoUsuario' data-nombre='$usuario->nombre'>";
				
				$this->session->set_flashdata('messages', $mensaje);
				// crearMensaje('success','fas fa-check','Éxito', $mensaje);
				redirect(base_url("sesion/login"));
	
			} else {
				renderLogin('sesion/nuevoCliente');
			}

			// renderLogin('sesion/nuevoCliente');
		}

		public function reestablecerContrasena($tokenRecuperacion){
			
			$helper = $this->verificarUsuarioPorTokenrec($tokenRecuperacion);

			if ($helper->estado){
				$vista = new stdClass();
				$vista->tokenRec = $tokenRecuperacion;
				$vista->nombre = $helper->nombreUsuario;
				renderLogin('sesion/reestablecerContrasena', $vista);
			}else{
				show_404();
			}
		}
}
