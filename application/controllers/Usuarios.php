<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('process/usuarios.php');

class Usuarios extends UsuariosProcess {
    
    public $nombre;
    private $sistema;

    function __construct(){
        parent::__construct();
        $this->nombre = 'usuarios';
        $this->sistema = getSistema();
    }

	public function index()
	{
        verificarPermisoExistencia($this->sistema, "ACCE");

        $get                = (object)$this->input->get();
        $get->segmento      = strlen($this->uri->segment(3)) > 10 ? "" : $this->uri->segment(3); 
        $get->permisosRoles = cargarPermisos($this->sistema);

        $vista = $this->procesarIndex($get, $this->sistema);

        render('usuarios/index', $vista);
    }
    
    public function nuevo(){
                
        $esDisponible = verificarPermisoExistencia($this->sistema, "CREA");
        
        $vista            = $this->procesarNuevo();
        $vista->urlAccion = 'usuarios/registrarNuevo';

        render('usuarios/formulario', $vista); 
    }

    public function registrarNuevo(){
        
        $post   = (object)$this->input->post();
        $helper = $this->registrarNuevoProces($post);

        redireccionar($helper, $this);
   
    }

    public function editar($idUsuario){
       
        $esDisponible = verificarPermisoExistencia($this->sistema, "MODI", $this->nombre, $idUsuario);

        if ($esDisponible) {
            
            $helper = $this->procesarEditar($idUsuario);
            
            if ($helper->estado) {
                $helper->urlAccion = 'usuarios/registrarEditar/'.$idUsuario;
                render('usuarios/formulario', $helper); 
            } else {
                redirect('usuarios');
            }
        } else {
            redirect('usuarios');
        }
    }

    public function registrarEditar($idUsuario){
        
        $post   = (object)$this->input->post();
        $helper = $this->registrarEditarProces($idUsuario, $post);
        
        redireccionar($helper, $this, true, $idUsuario);
    }

    public function eliminar($idUsuario){
           
        $esDisponible = verificarPermisoExistencia($this->sistema, "ELIM", $this->nombre, $idUsuario);

        if ($esDisponible) {
            $helper = $this->eliminarProces($idUsuario);

            redireccionar($helper, $this);

        } else {
            redirect('usuarios');
        }
    }
 
}
