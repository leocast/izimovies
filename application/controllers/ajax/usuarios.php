<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model("model_usuarios", 'usuarios');
    
    }

	public function informacionAcceso(){
        
        $post     = (object)$this->input->post();
        $respuesta = new AjaxResponse();
        
        if (!isset($post->idUsuario)) {
            $respuesta->estado = false;

            $respuesta->mensaje = "No se identificó el usuario'";
            echo json_encode($respuesta);
            die();
        }

        $vista             = new stdClass();
        $vista->infoAcceso = $this->usuarios->getInformacionAcceso($post->idUsuario);
  
        if (empty($vista->infoAcceso)) {
            $respuesta->estado  = false;
            $respuesta->mensaje = "No se identificó el usuario'";
            echo json_encode($respuesta);
            die();
        }

        $result       = new stdClass();
        $result->html = $this->load->view("usuarios/modal/informacionAcceso", $vista, true);

        $respuesta->estado  = true;
        $respuesta->mensaje = "";
        $respuesta->result  = $result;
        echo json_encode($respuesta);
    }
}

?>