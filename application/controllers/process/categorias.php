<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

class CategoriasProcess extends API_Controller {

    private $imagenConfig;
    public $nombreModulo;

    function __construct()
    {
        parent::__construct();
      
        $this->load->model('Model_categorias',  'categorias');
        $this->load->model('Model_roles',  'roles');
        
        $this->nombreModulo = 'categorias';
    }

    function procesarIndex($get, $sistema){

        $vista = new stdClass();

        $busquedaPaginacion = $get->segmento;
        $paginacion         = paginacionIndex(@$get->buscar, $sistema, $this->categorias);
        
        $data               = $this->categorias->getCategorias($paginacion['per_page'], $busquedaPaginacion, @$get->buscar);

        $vista->permisos    = $get->permisosRoles;
        $vista->buscar      = @$get->buscar;
        $vista->totalReg    = $paginacion['total_rows'];
        $vista->actualReg   = count($data);
        $vista->categorias  = $data;
         
        $this->pagination->initialize($paginacion);
        
        return $vista;
    }
    
    function procesarNuevo(){
        
        $vista = new stdClass();
     
        return $vista;
    }

    function registrarNuevoProces($post){
        $helper = new Helper();
        $campos =  $this->categorias->campos;

        aplicarReglas($campos, $post);
        if($this->form_validation->run()){

            $categoria = new stdClass();

            $categoria->id      = nuevoId('categorias');
            $categoria->nombre  = $post->nombre;
            
            $helper = $this->agregarDB($categoria);

        } else {
            $helper->estado = false;
            $helper->mensaje = "validacion";
        }

        return $helper;
    }

    function procesarEditar($id){

        $categoria    = $this->categorias->get($id);
        
        $vista = new stdClass();
      
        $vista->id              = $categoria->id;
        $vista->nombre          = $categoria->nombre;

        return $vista;
    }

    function registrarEditarProces($id, $post){
        
        $helper = new Helper();
        $campos =  $this->categorias->campos;

 
        aplicarReglas($campos, $post, $id, 'categorias');
            
        if($this->form_validation->run()){

            $categoria = $this->categorias->get($id);
            if(empty($categoria)) show_404();

            $categoria = new stdClass();
            $categoria->id         = $id;
            $categoria->nombre    = $post->nombre;

            $helper = $this->editarDB($id, $categoria);

        } else {
            $helper->estado     = false;
            $helper->mensaje    = "validacion";
        }

        return $helper;
    }

    function eliminarProces($id){

        $helper = new Helper();
       
        $helper = $this->eliminarDB($id);
        
        return $helper;
    }

    //FUNCIONES BD
    private function agregarDB($categoria){

        $helper  = new Helper();

        if($this->categorias->agregar($categoria) == 0) {

            $helper->estado  = true;
            $helper->mensaje = "Se ha guardado la categoría correctamente";

        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al registrarla categoría, contacte con un administrador";
        }

        return $helper;
    }

    private function editarDB($id, $categoria){
        
        $helper  = new Helper();
        $errores = false;

        //ACTUALIZAR INFO DEL USUARIO EN BD
        if($this->categorias->actualizar($id, $categoria) == 0) {
            $helper->estado  = true;
            $helper->mensaje = "Se ha la categoría correctamente";
        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al la categoría, contacte con un administrador";
        }
        return $helper;
    }

    private function eliminarDB($id){
        $helper  = new Helper();
        $errores = false;

        //ASEGURAR QUE NO ES EL ULTIMO ADMINISTRADOR
        if($this->categorias->eliminar($id) == 0) {
        
            $helper->estado  = true;
            $helper->mensaje = "Se ha eliminado la categoría correctamente.";
       
        } else {
       
            $helper->estado  = false;
            $helper->mensaje = "Ha ocurrido un error al eliminar la categoría.";
       
        };

        return $helper;
    }

    function getCategoria($id){
        return $this->categoria->get($id);
    }
}

?>