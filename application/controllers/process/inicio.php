<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class InicioProcess extends CI_Controller {

	public function renderIndex(){

        // update_concurrence("ACCE",$this->system,$this->session->userdata('user')['id']); CONCURRENCIA
        $usuario = $this->session->userdata('usuario')->nombre;

        $hora    = getdate()["hours"];
        $usuario = $usuario;

        $vista = new stdClass();

        if ($hora<4)      $vista->saludo = (object)['texto' => sprintf("Buenas noches %s, ¿Aún despierto?", $usuario), 'icono' => "cloudy-night-1"];
        elseif ($hora<6)  $vista->saludo = (object)['texto' => sprintf("Buenos días %s, ¿Despierto tan temprano?", $usuario), 'icono' => "cloudy-day-1"];
        elseif($hora<12)  $vista->saludo = (object)['texto' => sprintf(" Buenos días %s", $usuario), 'icono' => "day"];
        elseif($hora<=18) $vista->saludo = (object)['texto' => sprintf("Buenas Tardes %s", $usuario), 'icono' => "day"];
        else              $vista->saludo = (object)['texto' => sprintf("Buenas Noches %s", $usuario), 'icono' => "night"];

        //Dando formato
        $vista->saludo->hoy        = strtr(date('l, j \d\e F \d\e\l Y'), config_item('date_es')).".";
        $vista->saludo->rutaImagen = 'assets/img/weather/'.$vista->saludo->icono.'.svg';
      
        render('inicio/index', $vista);
    }
}

?>