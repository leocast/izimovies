<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

class PeliculasProcess extends API_Controller {

    private $imagenConfig;
    public $nombreModulo;

    function __construct()
    {
        parent::__construct();
      
        $this->load->model('Model_registrarPeliculas',   'peliculas');
        $this->load->model('Model_categorias',  'categorias');
        
        $this->nombreModulo = 'peliculas';
    }

    function procesarIndex($get, $sistema){

        $vista = new stdClass();

        $busquedaPaginacion = $get->segmento;
        $paginacion         = paginacionIndex(@$get->buscar, $sistema, $this->peliculas);
        
        $data               = $this->peliculas->getAll($paginacion['per_page'], $busquedaPaginacion, @$get->buscar);

        $vista->permisos    = $get->permisosRoles;
        $vista->buscar      = @$get->buscar;
        $vista->totalReg    = $paginacion['total_rows'];

        $vista->actualReg   = count($data);

        $vista->peliculas  = $data;
         
        $this->pagination->initialize($paginacion);
        
        return $vista;
    }

    function procesarVer($id){
        $pelicula = $this->peliculas->get($id);

        
        $vista = new stdClass();
        $vista->pelicula = (object)$pelicula;
        
        return $vista;
    }
}

?>