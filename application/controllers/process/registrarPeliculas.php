<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

class RegistrarPeliculasProcess extends API_Controller {

    private $imagenConfig;
    public $nombreModulo;

    function __construct()
    {
        parent::__construct();
      
        $this->load->model('Model_registrarPeliculas',   'peliculas');
        $this->load->model('Model_categorias',  'categorias');
        
        $this->nombreModulo = 'registrarpeliculas';
    }

    function procesarIndex($get, $sistema){

        $vista = new stdClass();

        $busquedaPaginacion = $get->segmento;
        $paginacion         = paginacionIndex(@$get->buscar, $sistema, $this->peliculas);
        
        $data               = $this->peliculas->getAll($paginacion['per_page'], $busquedaPaginacion, @$get->buscar);

        $vista->permisos    = $get->permisosRoles;
        $vista->buscar      = @$get->buscar;
        $vista->totalReg    = $paginacion['total_rows'];

        $vista->actualReg   = count($data);

        $vista->peliculas  = $data;
         
        $this->pagination->initialize($paginacion);
        
        return $vista;
    }
    
    function procesarNuevo(){
        
        $vista = new stdClass();

        $vista->categorias = selectFormato($this->categorias->getListaCategorias(),"id","nombre");

        $calidades = $this->peliculas->getListaCalidades();

        foreach ($calidades as $i => $calidad) {
            $calidades[$i]['nombre'] = "<br>". $calidad['subfijo'] . "</br> | ". $calidad['nombre'];
        }

        $vista->calidades = selectFormato($calidades,"id","nombre");
     
        return $vista;
    }

    function registrarNuevoProces($post){
        $helper = new Helper();
        $campos =  $this->peliculas->campos;

        aplicarReglas($campos, $post);
        if($this->form_validation->run()){

            $pelicula = new stdClass();

            // $pelicula->id           = nuevoId('peliculas');
            $pelicula->idCategoria  = $post->idCategoria;
            $pelicula->titulo       = $post->titulo;
            $pelicula->sinopsis     = $post->sinopsis;
            $pelicula->duracion     = $post->duracion;
            $pelicula->trailer      = $post->trailer;
            $pelicula->poster       = $post->poster;
            $pelicula->url          = $post->url;

            $helper = $this->agregarDB($pelicula);

            if ($helper->estado) {
               $peliculaID = $this->peliculas->getTopId();

               debug($peliculaID);
               
               foreach ($post->calidades as $key => $calidad) {
                    $calidadPelicula['idCalidad']  = $calidad;
                    $calidadPelicula['idPelicula'] = $peliculaID->id;

                    $this->peliculas->agregarCalidadesPeliculas($calidadPelicula);
                }

            }
          
        } else {
            $helper->estado = false;
            $helper->mensaje = "validacion";
        }

        return $helper;
    }

    function procesarEditar($id){

        $pelicula    = $this->peliculas->get($id);
        
        $vista = new stdClass();
      
        $vista->id          = $pelicula->id;     
        $vista->idCategoria = $pelicula->idCategoria;
        $vista->titulo      = $pelicula->titulo;
        $vista->sinopsis    = $pelicula->sinopsis;   
        $vista->duracion    = $pelicula->duracion;   
        $vista->trailer     = $pelicula->trailer;    
        $vista->poster      = $pelicula->poster;    
        $vista->url         = $pelicula->url;     
        $vista->url4k       = $pelicula->url4k;  

        $vista->calidadesSelect = selectMultipleValues($pelicula->calidades, 'id'); 
        
        $vista->categorias      = selectFormato($this->categorias->getListaCategorias(),"id","nombre");
        
        $calidades = $this->peliculas->getListaCalidades();

        foreach ($calidades as $i => $calidad) {
            $calidades[$i]['nombre'] = "<br>". $calidad['subfijo'] . "</br> | ". $calidad['nombre'];
        }

        $vista->calidades = selectFormato($calidades,"id","nombre");
        return $vista;
    }

    function registrarEditarProces($id, $post){
        
        $helper = new Helper();
        $campos =  $this->peliculas->campos;

        aplicarReglas($campos, $post, $id, 'peliculas');
            
        if($this->form_validation->run()){

            $pelicula = $this->peliculas->get($id);
            if(empty($pelicula)) show_404();

            $pelicula = new stdClass();
            $pelicula->id           = $id;
            $pelicula->idCategoria  = $post->idCategoria;
            $pelicula->titulo       = $post->titulo;
            $pelicula->sinopsis     = $post->sinopsis;
            $pelicula->duracion     = $post->duracion;
            $pelicula->trailer      = $post->trailer;
            $pelicula->poster       = $post->poster;
            $pelicula->url          = $post->url;

            $helper = $this->editarDB($id, $pelicula);

            if ($helper->estado) {

            $this->peliculas->eliminarCalidadesPelicula($id);
               
               foreach ($post->calidades as $key => $calidad) {
                    $calidadPelicula['idCalidad']  = $calidad;
                    $calidadPelicula['idPelicula'] = $id;

                    $this->peliculas->agregarCalidadesPeliculas($calidadPelicula);
                  
                }

            }
         
        } else {
            $helper->estado     = false;
            $helper->mensaje    = "validacion";
        }

        return $helper;
    }

    function eliminarProces($id){

        $helper = new Helper();
       
        $helper = $this->eliminarDB($id);
        
        return $helper;
    }

    //FUNCIONES BD
    private function agregarDB($pelicula){

        $helper  = new Helper();

        if($this->peliculas->agregar($pelicula) == 0) {

            $helper->estado  = true;
            $helper->mensaje = "Se ha guardado la película correctamente";

        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al registrar la película, contacte con un administrador";
        }

        return $helper;
    }

    private function editarDB($id, $pelicula){
        
        $helper  = new Helper();
        $errores = false;

        //ACTUALIZAR INFO DEL USUARIO EN BD
        if($this->peliculas->actualizar($id, $pelicula) == 0) {
            $helper->estado  = true;
            $helper->mensaje = "Se ha guardado la película correctamente";
        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al registrar la película, contacte con un administrador";
        }
        return $helper;
    }

    private function eliminarDB($id){
        $helper  = new Helper();
        $errores = false;

        //ASEGURAR QUE NO ES EL ULTIMO ADMINISTRADOR
        if($this->peliculas->eliminar($id) == 0) {
        
            $helper->estado  = true;
            $helper->mensaje = "Se ha eliminado la película correctamente.";
       
        } else {
       
            $helper->estado  = false;
            $helper->mensaje = "Ha ocurrido un error al eliminar la película.";
       
        };

        return $helper;
    }

    function getpelicula($id){
        return $this->pelicula->get($id);
    }
}

?>