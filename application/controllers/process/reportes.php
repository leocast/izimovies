<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportesProcess extends CI_Controller {
    
    private $sistema;
    public $nombreModulo;

    private $xlsGlobal;
    private $activeSheet;
    private $sheet;

    function __construct()
    {
        parent::__construct();

        $this->sistema = getSistema();
        $this->nombreModulo = 'reportes';

        $this->load->library('Xls');
        $this->load->model("model_usuarios","usuarios");

        $this->xlsGlobal   = $this->xls;
        $this->sheet       =  $this->xls->spreadsheet;
        $this->activeSheet = $this->sheet->getActiveSheet();

    }

    function renderPorTipo($post){
        
        $helper = new Helper();
        
        switch ($post->tipo) {
            case 'REPU':
                $helper = $this->generarREPU($post);
                break;

            case 'REPU_XLS':
                $helper = $this->generarREPU_XLS($post);
                break;
        }

        return $helper;
    }

    function generarREPU($post){
        $helper = new Helper();

        $helper->datosReporte = $this->usuarios->getUsuarios(50,1);
        
        if (empty($helper->datosReporte)) {
            $helper->estado = false;
            $helper->mensaje = "No hay datos que mostrar";
        }

        $helper->estado     = true;
        $helper->tipo       = $post->tipo;
        $helper->parametros = $this->getParametros($post);

        return $helper;
    }

    function generarREPU_XLS(){
        $filename = 'Reporte prueba' . date('Y-m-d');
 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
        header('Cache-Control: max-age=0');
        
        $this->xls->writer->save('php://output');
    }
    function getParametros($post){
        unset($post->tipo);
        return $post;
    }


    //CUSTOMIZE REPORT FUNCTIONS
    function center_alignment($cell){
        $this->activeSheet->getStyle($cell)->getAlignment()->applyFromArray(
            array('horizontal' => $this->xlsGlobal->style("alignment")::HORIZONTAL_CENTER)
        );
    }

    function vertical_alignment($cell){
        $this->activeSheet->getStyle($cell)->getAlignment()->applyFromArray(
            array('vertical' => $this->xlsGlobal->style("alignment")::VERTICAL_CENTER)
        );
    }

    function cell_background($cell, $color){
        $this->activeSheet->getStyle($cell)->getFill()
        ->setFillType($this->xlsGlobal->style("Fill")::FILL_SOLID)->getStartColor()->setRGB($color);
    }

    function set_font($cell, $fontArray){
        $this->activeSheet->getStyle($cell)->applyFromArray($fontArray);
    }

    function column_autosize($range1, $range2){
        foreach(range($range1,$range2) as $columnID) {
            $this->activeSheet->getColumnDimension($columnID)->setAutoSize(true);
        }
    }

    function set_border($cells, $color){
        $this->activeSheet->getStyle($cells)->getBorders()->applyFromArray(
            [
                'bottom' => [
                    'borderStyle' => $this->xlsGlobal->style('Border')::BORDER_THIN,
                    'color' => [
                        'rgb' => $color
                    ]
                ],
                'top' => [
                    'borderStyle' => $this->xlsGlobal->style('Border')::BORDER_THIN,
                    'color' => [
                        'rgb' => $color
                    ]
                ],
                'left' => [
                    'borderStyle' => $this->xlsGlobal->style('Border')::BORDER_THIN,
                    'color' => [
                        'rgb' => $color
                    ]
                ],
                'right' => [
                    'borderStyle' => $this->xlsGlobal->style('Border')::BORDER_THIN,
                    'color' => [
                        'rgb' => $color
                    ]
                ]
            ]
        );
    
    }

    function set_borders($cellsArray, $start, $end, $color){
        foreach ($cellsArray as $i => $value) {
            for ($_i=$start; $_i <= $end; $_i++) { 
                $this->set_border($value . $_i, $color);
            }
        }
    }
}

?>