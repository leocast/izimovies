<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';
class RolesProcess extends API_Controller {

    public $nombreModulo;

    function __construct()
    {
        parent::__construct();

        $this->nombreModulo = 'roles';

        $this->load->model('model_roles'    ,  'roles');
        $this->load->model('model_acciones' ,  'acciones');
        $this->load->model('model_modulos'  ,  'modulos');
        $this->load->model('model_permisos' ,  'permisos');
    }

    function procesarIndex($get, $sistema){

        $vista = new stdClass();

        $busquedaPaginacion = $get->segmento;

        $paginacion         = paginacionIndex(@$get->buscar, $sistema, $this->roles);
        $roles              = $this->roles->getRoles($paginacion['per_page'], $busquedaPaginacion, @$get->buscar);
        
        $vista->permisos    = $get->permisosRoles;
        $vista->buscar      = @$get->buscar;
        $vista->totalReg    = $paginacion['total_rows'];
        $vista->actualReg   = count($roles);
        $vista->roles       = $roles;
         
        $this->pagination->initialize($paginacion);
        
        return $vista;
    }
    
    function procesarNuevo(){
        
        $vista = new stdClass();
      
        $vista->active         = 'checked';
        $vista->actives_checks = [];
        
        $vista->modulos  = $this->modulos->getModulos();
        $vista->acciones = $this->acciones->getAcciones();
        return $vista;
    }

    function registrarNuevoProces($post){

        $helper = new Helper();
        $campos =  $this->roles->campos;

        aplicarFormato($campos);
        aplicarReglas($campos, $post);
        
        if($this->form_validation->run()){
            
            $rol = new stdClass();
            
            $rol->id          = nuevoId('roles');
            $rol->nombre      = $post->nombre;
            $rol->creacion    = date("Y-m-d H:i:s");
            $rol->activo      = (isset($post->activo)) ? true: false;
            
            $activos = $this->validarAcciones($post);
            
            if (empty($activos)) {
                $helper->estado = false;
                $helper->mensaje = "No es posible registrar un rol sin ningún permiso";
            } else {
                $helper = $this->agregarRol($rol, $activos);
            }

        } else {
            $helper->estado = false;
            $helper->mensaje = "validacion";
        }

        return $helper;
    }

    function procesarEditar($idRol){
        
        $vista  = new stdClass();
        $rol    = $this->getRol($idRol);

        $vista->id             = $rol->id;
        $vista->nombre         = $rol->nombre;
        $vista->active         = $rol->activo == true ? 'checked': '';
        $vista->actives_checks = [];
        $vista->estado         = true;

        $permisos = $this->permisos->getAccionesPorRol($rol->id);

        //PREPARAR PERMISOS PARA LA VISTA
        foreach ($permisos AS $permiso)
            $vista->actives_checks[$permiso['idaccion']] = true;

        $vista->modulos  = $this->modulos->getModulos();
        $vista->acciones = $this->acciones->getAcciones();
    
        return $vista;
    }

    function registrarEditarProces($idRol, $post){
        
        $helper = new Helper();
        $campos =  $this->roles->campos;

        aplicarFormato($campos);
        aplicarReglas($campos, $post, $idRol, 'roles');
        
        if($this->form_validation->run()){

            $rol = new stdClass();
            
            $rol->nombre = $post->nombre;
            $rol->activo = isset($post->activo) ? true : false;
            
            //VALIDAR QUE LA ACCION EXISTA EN LA BD
            $activos = $this->validarAcciones($post);

            $helper = $this->editarRol($idRol, $rol, $activos);
            
        } else {
            $helper->estado     = false;
            $helper->mensaje    = "validacion";
        }

        return $helper;
    }

    function eliminarProces($idRol){

        //ASEGURAR QUE NO ES EL ROL ADMINISTRADOR
         if($idRol == config_item('idRolAdmin')){
             $helper->estado  = false;
             $helper->mensaje = "No es posible eliminar el rol de administrador";

            //VERIFICAR QUE NO ESTE ASIGNADO
        } else if($this->roles->tieneDependencias($idRol)) {
             $helper->estado  = false;
             $helper->mensaje = "No es posible eliminar el rol <strong>". $rol->nombre . "</strong> porque tiene usuarios asociados";

         //ELIMINAR ROL
        } else {
            $helper = $this->eliminarRol($idRol);
            $helper->esDisponible = $esDisponible;
        }

        return $helper;
    }

    //FUNCIONES LOCALES
    private function validarAcciones($post){
        $activos = [];
        foreach ($post AS $key => $value){
            $checkbox = explode('-',$key);
            if($checkbox[0] == 'accion_ident')
                if($this->acciones->esAccionValida($checkbox[1]))
                    array_push($activos,$checkbox[1]);
        }

        return $activos;
    }

    //FUNCIONES BD
    private function agregarRol($rol, $activos){

        $helper  = new Helper();
        $errores = false;

        if($this->roles->agregar($rol) == 0) {
            //ASIGNAR ACCIONES AL ROL
            foreach ($activos AS $activo){
                if($this->permisos->setAccionesARol($rol->id, $activo) == 1) $errores = true;
            }

            //COMPROBAR SI HUBO ERRORES
            if($errores){
                $helper->estado  = true;
                $helper->mensaje = "Ocurrió un error al registrar los permisos del rol, contacte con un administrador";
            } else {
                $helper->estado  = true;
                $helper->mensaje = "Se ha guardado el rol correctamente";
            }
            
        } else{
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al registrar el rol, contacte con un administrador";
        }
        
        return $helper;
    }

    private function editarRol($idRol, $rol, $activos){
        
        $helper  = new Helper();
        $errores = false;

        if($this->roles->actualizar($idRol, $rol) === 0) {

            //LIMPIAR PERMISOS ANTERIORES
            if($this->permisos->eliminarAccionesDeRoles($idRol) === 0){

                //GUARDAR NUEVAS ACCIONES AL ROL
                $errores = false;
                foreach ($activos AS $activo){
                    if($this->permisos->setAccionesARol($idRol, $activo) == 1) $errores = true;
                }

                //COMPROBAR SI HUBO ERRORES
                if($errores){
                    $helper->estado  = true;
                    $helper->mensaje = "Ocurrió un error al actualizar los permisos del rol, contacte con un administrador";
                } else {
                    $helper->estado  = true;
                    $helper->mensaje = "Se ha guardado el rol correctamente";
                }

            //NO SE PUDO ELIMINAR LAS ACCIONES DEL ROL
            } else{
                $helper->estado  = true;
                $helper->mensaje = "Ocurrió un error al eliminar los permisos del rol, contacte con un administrador";
            }
            //NO SE PUDO ACTUALIZAR LA INFO DEL ROL
        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al actualizar el rol, contacte con un administrador";
        }

        return $helper;
    }

    private function eliminarRol($idRol){
        $helper  = new Helper();
        $errores = false;

        //ELIMINAR ACCIONES
        if($this->permisos->eliminarAccionesDeRoles($idRol) == 0){
            //ELIMINAR ROL
            if($this->roles->eliminar($idRol) == 0){
               //COMPROBAR SI HUBO ERRORES
                $helper->estado  = true;
                $helper->mensaje = "Se ha eliminado el rol correctamente";
            } else {
                $helper->estado  = true;
                $helper->mensaje = "Ocurrió un error al eliminar el rol, contacte con un administrador";
            }
        } else{
            $helper->estado  = true;
            $helper->mensaje = "Ocurrió un error al eliminar los permisos del rol, contacte con un administrador";
        } 

        return $helper;
    }

    function getRol($idRol){
        return $this->roles->get($idRol); 
    }
}

?>