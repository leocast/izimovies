<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';
class SesionProcess extends API_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuarios','usuarios');
        $this->load->model('model_permisos','permisos');
    }

    function validarPost($post){
        $camposSesion =  $this->usuarios->camposSesion;
		aplicarReglas($camposSesion, $post);

		if($this->form_validation->run())
            return true;
        else
            return false;
    }

    function iniciarSesion($datos){
        
        $usuario = $this->usuarios->login(strtolower($datos->usuario), md5($datos->contrasena));

        if(!empty($usuario)){
            
            $token = nuevoToken();
            
            $usuario->permisos = $this->getPermisos($usuario);
            
            $this->session->set_userdata('usuario', $usuario);

            $edit = [
                'tokenaut' => $token,
                'plataforma' => 1,
                'ultimoacc' => date('Y-m-d H:i:s')
            ];

            //Guardamos el token nuevo
            $this->usuarios->actualizar($usuario->id, $edit);
            return true;
        } else { 
            return false;
        }
    }

    function verificarSesion(){

        $usuarioSesion = $this->session->userdata('usuario');

        $token = $this->usuarios->verificarEstadoToken($usuarioSesion->tokenaut);

        if($token === false)
           return true;
        else {
            return false;
        }
    }

    function cerrar(){
        $usuario = $this->session->userdata('usuario');
        $this->usuarios->actualizar($usuario->id,
        ['idultimaacc'  => null,
         'ultimorec'    => null,
         'concurrencia' => date('Y-m-d H:i:s'),
         'plataforma'   => 4
        ]);
    }

    function enviarEmailRecuperacion($post){
        
        $helper = new stdClass();
       
        $this->form_validation->set_rules('email', 'Correo Electrónico', 'required|valid_email');

        if ($this->form_validation->run()) {

            $usuario = $this->usuarios->getUsuarioPorEmail($post->email);

            if (empty($usuario)) {
                $helper->estado  = false;
                $helper->mensaje = "El usuario no existe";
                return $helper;
            }

            $tokenRecuperacion = nuevoToken('reset_password');

            $this->usuarios->actualizar($usuario->id, ['tokenrec' => $tokenRecuperacion]);
            $usuario->tokenrec = $tokenRecuperacion;

            $datosCorreo = new stdClass();
            $datosCorreo->correos = [$post->email];
            $datosCorreo->asunto = "Reestablecer contraseña";

            $enviado = enviarEmail('email/olvidoContrasena', $usuario, $datosCorreo);

            if ($enviado) {
                $helper->estado  = true;
                $helper->mensaje = "Se ha enviado un correo electronico con el enlace para reestablecer su contraseña, revise la bandeja de entrada de su correo electrónico y siga las instrucciones.";
            } else {
                $helper->estado  = false;
                $helper->mensaje = "Ha ocurrido un error al enviar el correo";
            }
            
        } else {
            $helper->estado  = false;
            $helper->mensaje = "El correo no contiene el formato correcto";
        }
        
        return $helper;
    }

    function reestablecerContrasenaProces($post){
        $helper = new stdClass();

        $usuario = $this->usuarios->getUsuarioPorTokenRec($post->tokenRec);

        if (empty($usuario)) {
            $helper->estado  = false;
            $helper->mensaje = "Ha ocurrido un error";
            return $helper;
        }

        if(isset($post->contrasena) and isset($post->ccontrasena)) {
            $this->form_validation->set_rules('contrasena', 'Contraseña', 'trim|required|min_length[6]|max_length[20]');
            $this->form_validation->set_rules('ccontrasena', 'Confirmar Contraseña', 'trim|matches[contrasena]');

            if ($this->form_validation->run()) {
                if ($this->usuarios->actualizar($usuario->id, ['contrasena' => md5($post->contrasena), 'tokenrec' => null]) == 0){
                    $helper->estado  = true;
                    $helper->mensaje = "Se ha cambiado la contraseña correctamente";
                } else {
                    $helper->estado  = false;
                    $helper->mensaje = "Ha ocurrido un error al cambiar la contraseña";
                }
            } else{
                $helper->estado  = false;
                $helper->mensaje = "Las contras no contienen el formatuky";
            }
        } 

        $helper->tokenRec = $post->tokenRec;        
        return $helper;
    }

    function verificarUsuarioPorTokenrec($tokenRecuperacion){
        $usuario = $this->usuarios->getUsuarioPorTokenRec($tokenRecuperacion);
        $helper  = new stdClass();
			if (empty($usuario)) {
                $helper->estado = false;
            }
            else{
                $helper->estado = true;
                $helper->nombreUsuario = $usuario->nombre;
            }

            return $helper;
    }
    //FUNCIONES LOCALES
    private function getPermisos($usuario){
        
        $permisos = [];
        $acciones = $this->permisos->getAccionesConPermisosPorRol($usuario->idrol, true);

        foreach ($acciones AS $i => $accion){
            $key = array_search($accion['nombre'], array_column($permisos, 'alias'));

            $accion['permiso'] = ($accion['permiso'] == 't') ? true : false;
            // $accion['accion'] = (strtolower(str_replace(' ', '_', $accion['accion'])));

            if ($key === false){
                $permisos[$accion['nombre']] = ['alias' => $accion['nombre'], 'acciones' => [$accion['identificador'] => $accion['permiso']]];
            }else{
                $permisos[$accion['nombre']]['acciones'][$accion['identificador']] = $accion['permiso'];
            }
        }

        foreach ($permisos as $i => $permiso){
            $permisos[$i] = $permiso['acciones'];
        }

        return $permisos;
    }

}

?>