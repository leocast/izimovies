<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';
class UsuariosProcess extends API_Controller {

    private $imagenConfig;
    public $nombreModulo;

    function __construct()
    {
        parent::__construct();
      
        $this->load->model('model_usuarios',  'usuarios');
        $this->load->model('model_roles',  'roles');
        
        $this->nombreModulo = 'usuarios';

        $this->imagenConfig['upload_path']      = './assets/img/perfil/';
        $this->imagenConfig['allowed_types']    = 'jpg|png|jpeg';
        $this->imagenConfig['max_size']         = 1024;
        $this->imagenConfig['overwrite']        = true;
        $this->imagenConfig['file_ext_tolower'] = true;
    }

    function procesarIndex($get, $sistema){

        $vista = new stdClass();

        $busquedaPaginacion = $get->segmento;
        $paginacion         = paginacionIndex(@$get->buscar, $sistema, $this->usuarios);
        
        $usuarios           = $this->usuarios->getUsuarios($paginacion['per_page'], $busquedaPaginacion, @$get->buscar);

        $vista->permisos    = $get->permisosRoles;
        $vista->buscar      = @$get->buscar;
        $vista->totalReg    = $paginacion['total_rows'];
        $vista->actualReg   = count($usuarios);
        $vista->usuarios    = $usuarios;
         
        $this->pagination->initialize($paginacion);
        
        return $vista;
    }
    
    function procesarNuevo(){
        
        $vista = new stdClass();
        $vista->imagenPerfil = "default.jpg";
        $vista->activo       = 'checked';
        $vista->listaRoles   = selectFormato($this->roles->getListaRoles(),"id","nombre");

        return $vista;
    }

    function registrarNuevoProces($post){
        $helper = new Helper();
        $campos =  $this->usuarios->camposUsuarios;

        aplicarReglas($campos, $post);
        if($this->form_validation->run()){

            $usuario = new stdClass();

            $usuario->id         = nuevoId('usuarios');
            $usuario->usuario    = $post->usuario;
            $usuario->contrasena = md5($post->contrasena);
            $usuario->idrol      = $post->idRol;
            $usuario->nombre     = $post->nombre;
            $usuario->correo     = $post->correo;
            $usuario->activo     = (isset($post->activo)) ? true : false;
            $usuario->creacion   = date("Y-m-d H:i:s");
            
            //GESTIONAR IMAGEN
            $imagenSubida = true;
            $this->imagenConfig['file_name']      = $usuario->id;
           
            $this->load->library('upload', $this->imagenConfig);
          
            if (!$this->upload->do_upload('imagen')){
                $usuario->imagen = "default.jpg";
                $imagenSubida = false;
            }else{
                $usuario->imagen = $usuario->id.$this->upload->file_ext;
            }

            $helper = $this->agregarUsuario($usuario, $imagenSubida);

        } else {
            $helper->estado = false;
            $helper->mensaje = "validacion";
        }

        return $helper;
    }

    function procesarEditar($idUsuario){
        
        if (config_item('idAdminPrincipal') == $idUsuario) {
            $helper = new Helper();

            $helper->estado = false;
            $helper->mensaje = "No se puede editar el administrador principal";
            return $helper;
        }

        $usuario    = $this->usuarios->get($idUsuario);
        
        $vista = new stdClass();
      
        //PREPARAR VARIABLES PARA LA VISTA
        $vista->estado          = true;
        $vista->id              = $usuario->id;
        $vista->usuario         = $usuario->usuario;
        $vista->idRol           = $usuario->idrol;
        $vista->nombre          = $usuario->nombre;
        $vista->correo          = $usuario->correo;
        $vista->activo          = $usuario->activo == true ? 'checked' : '';
        $vista->imagenPerfil    = $usuario->imagen == '' ? 'default.jpg' : $usuario->imagen;

        $vista->listaRoles   = selectFormato($this->roles->getListaRoles(),"id","nombre");
        
        return $vista;
    }

    function registrarEditarProces($idUsuario, $post){
        
        $helper = new Helper();
        $campos =  $this->usuarios->camposUsuarios;

        //VERIFICAR CAMBIO DE CONTRASEÑA
        if($post->contrasena == '' AND $post->ccontrasena == '')
            unset($post->contrasena, $post->ccontrasena);
        
            aplicarReglas($campos, $post, $idUsuario, 'usuarios');
            
            if($this->form_validation->run()){

            $usuario = $this->usuarios->get($idUsuario);
            if(empty($usuario)) show_404();

            $usuario = new stdClass();
            //PREPARAR VARIABLES PARA LA BD
            $usuario->id         = $idUsuario;
            $usuario->usuario    = $post->usuario;
            $usuario->idrol      = $post->idRol;
            $usuario->nombre     = $post->nombre;
            $usuario->correo     = $post->correo;
            $usuario->activo     = isset($post->activo) ? true : false;

            //ASIGNAR NUEVA CONTRASEÑA
            if(isset($post->contrasena))
                $usuario->contrasena = md5($post->contrasena);

            //VERIFICAR QUE NO SEA EL ULTIMO ADMINISTRADOR
            if($this->usuarios->esUltimoAdministrador($idUsuario)){
                $this->session->set_flashdata('messages_extra',set_message('edit_last_admin'));
                if($usuario->activo == 'f') $usuario->activo = 't';
                if($usuario->idrol != config_item('idRolAdmin'))
                    $usuario->idrol = config_item('idRolAdmin');
            }

            //GESTIONAR IMAGEN
            $imagenSubida = true;
            $this->imagenConfig['file_name'] = $usuario->id;
            $this->load->library('upload', $this->imagenConfig);
            
            if (!$this->upload->do_upload('imagen')){
                $usuario->imagen = "default.jpg";
                $image_uploaded = false;
            }else{
                $usuario->imagen = $usuario->id.$this->upload->file_ext;
            }

            $helper = $this->editarUsuario($idUsuario, $usuario, $imagenSubida);

        } else {
            $helper->estado     = false;
            $helper->mensaje    = "validacion";
        }

        return $helper;
    }

    function eliminarProces($idUsuario){

        $helper = new Helper();
        $usuario    = $this->usuarios->get($idUsuario);

        //VERIFICAR QUE NO SEA EL ULTIMO ADMINISTRADOR
        if($this->usuarios->esUltimoAdministrador($idUsuario)){
            if($usuario->activo == 'f') 
                $usuario->activo = 't';
            
            if($usuario->idrol != config_item('idRolAdmin'))
                $usuario->idrol = config_item('idRolAdmin');
        
            $helper->estado       = false;
            $helper->mensaje      = "No se puede eliminar el último administrador";
            return $helper;
        }

        $helper = $this->eliminarUsuario($idUsuario);
        
        return $helper;
    }

    //FUNCIONES BD
    private function agregarUsuario($usuario, $imagenSubida){

        $helper  = new Helper();

        if($this->usuarios->agregar($usuario) == 0) {

            if($imagenSubida){
                $helper->estado  = true;
                $helper->mensaje = "Se ha guardado el usuario correctamente";
            } else {
                $helper->estado  = true;
                $helper->mensaje = "Se ha establecido una imagen de perfil por defecto, se ha guardado el usuario correctamente";
            }

        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al registrar el usuario, contacte con un administrador";
        }

        return $helper;
    }

    private function editarUsuario($idUsuario, $usuario, $imagenSubida){
        
        $helper  = new Helper();
        $errores = false;

        //ACTUALIZAR INFO DEL USUARIO EN BD
        if($this->usuarios->actualizar($idUsuario, $usuario) == 0) {
            if($imagenSubida){
                $helper->estado  = true;
                $helper->mensaje = "Se ha guardado el usuario correctamente";
            } else {
                $helper->estado  = true;
                $helper->mensaje = "Se ha establecido una imagen de perfil por defecto, se ha guardado el usuario correctamente";
            }
        } else {
            $helper->estado  = false;
            $helper->mensaje = "Ocurrió un error al registrar el rol, contacte con un administrador";
        }
        return $helper;
    }

    private function eliminarUsuario($idUsuario){
        $helper  = new Helper();
        $errores = false;

        //ASEGURAR QUE NO ES EL ULTIMO ADMINISTRADOR
        if($this->usuarios->esUltimoAdministrador($idUsuario)){
          
            $helper->estado  = false;
            $helper->mensaje = "No es posible eliminar el único Administrador del sistema.";
        
        } else if($this->usuarios->eliminar($idUsuario) == 0) {
        
            $helper->estado  = true;
            $helper->mensaje = "Se ha eliminado el usuario correctamente.";
       
        } else {
       
            $helper->estado  = false;
            $helper->mensaje = "Ha ocurrido un erro al eliminar el usuario.";
       
        };

        return $helper;
    }

    function getUsuario($idUsuario){
        return $this->usuarios->get($idUsuario);
    }
}

?>