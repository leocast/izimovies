<?php
if(!function_exists('aplicarFormato'))
{
    function aplicarFormato($campos)
    {
        $post =& $_POST;
        foreach ($campos as $campo => $attr){
            if(key_exists($campo,$post)){
                switch ($attr['formato']){
                    case 'ucwords':
                        $post[$campo] = ucwords($post[$campo]);
                        break;
                    case 'ucfirst':
                        $post[$campo] = ucfirst($post[$campo]);
                        break;
                    case 'strtolower':
                        $post[$campo] = strtolower($post[$campo]);
                        break;
                    case 'strtoupper':
                        $post[$campo] = strtoupper($post[$campo]);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

if(!function_exists('aplicarReglas'))
{
    function aplicarReglas($reglas, $form, $own_id = null,$modelo = null)
    {
        $CI =& get_instance();
        $modelo = ($modelo != null)?$modelo:$CI->uri->segment(1);
        foreach ($form as $campo => $valor){
            if(!key_exists($campo,$reglas)) continue;
            if(!key_exists('unique',$reglas[$campo])){
                $CI->form_validation->set_rules($campo,$reglas[$campo]['nombre'],$reglas[$campo]['reglas']);
                continue;
            }
            else if($own_id !== null and $modelo !== null){
                $CI->load->model("model_" . $modelo, 'new_model');
                if ($CI->new_model->is_available($valor, $own_id))
                    $reglas[$campo]['unique'] = '';
            }
            $CI->form_validation->set_rules($campo,$reglas[$campo]['nombre'],$reglas[$campo]['reglas'].$reglas[$campo]['unique']);
        }
    }
}

if(!function_exists('selectFormato'))
{
    function selectFormato($array, $clave, $valor, $concat = null)
    {
        
        if(empty($array)) return [];
        $temp = [];
        foreach ($array as $item){
            $temp[trim($item[$clave])] = trim($item[$valor]).((!empty($concat))?" ".trim($item[$concat]):"");
        }
        return $temp;
    }
}


if(!function_exists('selectFormatoMultiple'))
{
    function selectMultipleValues($array, $key)
    {
        $temp = [];

        foreach ($array as $i => $value) {
            $temp[] = $value->$key;
        }

        return $temp;
    }
}

if(!function_exists('select2'))
{
    function select2($array, $id, $text, $concat = null, $no_option = false)
    {
        if(empty($array)) return [];
        $temp = [];

        if ($no_option) 
            $temp[] = [
                "id" => "0",
                "text" => "",
            ];

        foreach ($array as $i => $item){
            
            if ($concat != null) 
                $textS2 = $item[$concat] . " " .  $item[$text];
             else 
                $textS2 = $item[$text];

            if ($i == 0) {
                $nuevoItem = [
                    "id"       => $item[$id],
                    "text"     => $textS2,
                    "selected" => true
                ];
            } else {
                $nuevoItem = [
                    "id" => $item[$id],
                    "text" => $textS2
                ];
            }
            
            $temp[] = $nuevoItem;
        }
        
        return $temp;
    }
}

if(!function_exists('menorQueHoy'))
{
    function menorQueHoy($fecha)
    {
        if(date("Y-m-d", strtotime($fecha)) < date("Y-m-d"))
            return false;
        return true;
    }
}

if(!function_exists('verificarFecha'))
{
     function verificarFecha($str) {
         if (preg_match("/(^[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$)/", $str)) {
            return true;
         } else return false;
    }
}