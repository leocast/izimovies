<?php

if(!function_exists('set_message'))
{
    function set_message($ident, $elemento = "Registro", $a = false, $icono = "fas fa-bell")
    {
        $a =! $a;
        $mensajes = [
            //ESPECIALES
            'edit_img' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-user',
                'title' => 'Modificar Usuario',
                'text'  => 'Cambios guardados correctamente, se estableció una imagen por defecto como perfil.'],
            'new_img' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-user',
                'title' => 'Nuevo Usuario',
                'text'  => 'Usuario creado correctamente, se estableció una imagen por defecto como perfil.'],
            'edit_actions_fail' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-exclamation-triangle',
                'title' => 'Modificar Rol',
                'text'  => 'No se han podido guardar las nuevas acciones del Rol.'],
            'del_actions_fail' => [
                'type'  => 'danger',
                'ico'   => 'fas fa-exclamation-triangle',
                'title' => 'Eliminar Rol',
                'text'  => 'No se han podido eliminar las acciones del Rol, Acción Cancelada.'],
            'edit_last_admin' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-ban',
                'title' => 'Único Administrador',
                'text'  => 'No se puede Inactivar o cambiar el rol del único Administrador del sistema.'],
            'order_empty' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-exclamation-triangle',
                'title' => 'Imposible crear el nuevo pedido',
                'text'  => 'Para crear un nuevo pedido, valla al módulo Guía de Visita o Pedidos.'],
            'payment_empty' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-exclamation-triangle',
                'title' => 'Imposible registrar un pago nuevo',
                'text'  => 'Ha ocurrido un error al registrar el pago.'],


            //GENERAL
            'sign_fail' => [
                'type'  => 'danger',
                'ico'   => 'fas fa-exclamation-triangle',
                'title' => 'Integridad Corrompida',
                'text'  => 'Se ha detectado una manipulación en el formulario. ¡Acción cancelada!'],
            'del_admin' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-ban',
                'title' => 'Rol Administrador Protegido',
                'text'  => 'No es posible eliminar el rol Administrador.'],
            'edit_admin' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-ban',
                'title' => 'Rol Administrador Protegido',
                'text'  => 'No es posible modificar el rol Administrador.'],
            'del_last_admin' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-ban',
                'title' => 'Único Administrador',
                'text'  => 'No es posible eliminar el único Administrador del sistema.'],
            'occupied_resource' => [
                'type'  => 'warning',
                'ico'   => 'fas fa-user-shield',
                'title' => 'Recurso Ocupado',
                'text'  => $elemento],

            //NUEVO
            'new' => [
                'type'  => 'success',
                'ico'   => $icono,
                'title' => (($a)?"Nuevo ":"Nueva "). $elemento,
                'text'  => "Se ha creado ". (($a)?" el ":" la ").$elemento." correctamente."],
            'new_fail' => [
                'type'  => 'danger',
                'ico'   => $icono,
                'title' => (($a)?"Nuevo ":"Nueva "). $elemento,
                'text'  => "Ocurrió un error al crear ". (($a)?" el ":" la ").$elemento."."],
            'new_partial' => [
                'type'  => 'warning',
                'ico'   => $icono,
                'title' => (($a)?"Nuevo ":"Nueva "). $elemento,
                'text'  => "Ocurrió un error al crear ". (($a)?" el ":" la ").$elemento.", La infomación no se ha guardado completamente."],

            //EDITAR
            'edit' => [
                'type'  => 'success',
                'ico'   => $icono,
                'title' => "Modificar ".$elemento,
                'text'  => "Se ha modificado ". (($a)?" el ":" la ").$elemento." correctamente."],
            'edit_fail' => [
                'type'  => 'danger',
                'ico'   => $icono,
                'title' => "Modificar ".$elemento,
                'text'  => "Ocurrió un error al modificar ". (($a)?" el ":" la ").$elemento."."],
            'edit_partial' => [
                'type'  => 'warning',
                'ico'   => $icono,
                'title' => "Modificar ".$elemento,
                'text'  => "Ocurrió un error al modificar ". (($a)?" el ":" la ").$elemento.", La infomación no se ha guardado completamente."],

            //ELIMINAR
            'del' => [
                'type'  => 'success',
                'ico'   => $icono,
                'title' => "Eliminar ".$elemento,
                'text'  => "Se ha eliminado ". (($a)?" el ":" la ").$elemento." correctamente."],
            'del_fail' => [
                'type'  => 'danger',
                'ico'   => $icono,
                'title' => "Eliminar ".$elemento,
                'text'  => "Ocurrió un error al eliminar ". (($a)?" el ":" la ").$elemento."."],
            'del_dep' => [
                'type'  => 'warning',
                'ico'   => $icono,
                'title' => "Eliminar ".$elemento,
                'text'  => "No se puede eliminar ". (($a)?" el ":" la ").$elemento." porque ya tiene movimientos asociados."],
            'del_partial' => [
                'type'  => 'warning',
                'ico'   => $icono,
                'title' => "Eliminar ".$elemento,
                'text'  => "No se pudo eliminar ". (($a)?" el ":" la ").$elemento." completamente."],

            //SESION
            'user_pass_incorrect' => [
                'type'  => 'danger',
                'ico'   => 'fas fa-bell',
                'title' => 'Error de Autenticación',
                'text'  => 'Usuario y/o Contraseña incorrectas.'],
            'email_forgot_incorrect' => [
                'type'  => 'danger',
                'ico'   => 'fas fa-bell',
                'title' => 'Correo electrónico Invalido',
                'text'  => 'El correo electrónico especificado no se encuentra.'],
            'email_sent' => [
                'type'  => 'success',
                'ico'   => 'fas fa-bell',
                'title' => 'Correo electrónico enviado',
                'text'  => 'Se ha enviado un correo electronico con el enlace para reestablecer su contraseña, revise la bandeja de entrada de su correo electrónico y siga las instrucciones.'],
            'email_sent_fail' => [
                'type'  => 'danger',
                'ico'   => 'fas fa-bell',
                'title' => 'Falló el envio de correo electronico',
                'text'  => 'Hubo un error al enviar el correo electrónico, intentelo de nuevo.'],
            'password_changed' => [
                'type'  => 'success',
                'ico'   => 'fas fa-bell',
                'title' => 'Contraseña cambiada',
                'text'  => 'Se ha cambiado la contraseña correctamente, inicia sesión con las nuevas credenciales de acceso.'],
            'password_changed_fail' => [
                'type'  => 'danger',
                'ico'   => 'fas fa-bell',
                'title' => 'Falló el cambio de contraseña',
                'text'  => 'Hubo un error al realizar el cambio de contraseña, intentelo de nuevo.'],
        ];

        if(!key_exists($ident, $mensajes)){
            return "<div class='alert alert-danger alert-dismissible fade show shadow'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>×</span></button><h4 class='alert-heading'><span class='fas fa-exclamation-triangle'></span> Mensaje no encontrado!</h4><p>Codigo: ".$ident."</p> </div>";
        }else{
            return "<div class='alert alert-".$mensajes[$ident]['type']." alert-dismissible fade show shadow'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                        <h4 class='alert-heading'><span class='".$mensajes[$ident]['ico']."'></span> ".$mensajes[$ident]['title']."</h4><p>".$mensajes[$ident]['text']."</p></div>";
        }
    }
}
if(!function_exists('crearMensaje'))
{
    function crearMensaje($tipo, $icono, $titulo, $texto)
    {
        $CI =& get_instance();

        $datosVista = new stdClass();
        $datosVista->tipo   = $tipo;
        $datosVista->icono  = $icono;
        $datosVista->titulo = $titulo;
        $datosVista->texto  = $texto;
        
        $mensaje = $CI->load->view('web/mensaje', $datosVista, true);
        
        if ($texto != 'validacion') {
            $CI->session->set_flashdata('messages', $mensaje);
        }
    }
}