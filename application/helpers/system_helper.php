<?php
if(!function_exists('getSistema'))
{
    function getSistema()
    {
        $CI =& get_instance();
        $CI->load->model('model_modulos','modulo');

        $modulo = $CI->modulo->getModulo($CI->uri->segment(1));
        
        $uri2 = $CI->uri->segment(2);
        $uri3 = $CI->uri->segment(3);

        $accion = generarAccion($uri2, $uri3);

        $sistema = new stdClass();
        $sistema->app_nombre         = config_item('app_name');
        $sistema->pluginsGenerales   = config_item('plugins')['generales'];
        $sistema->accion_uri         = ($uri2 !== null)? $uri2:'index';
        $sistema->accion             = $accion;
        $sistema->modulo             = $modulo;
        $sistema->busqueda           = false;
        $sistema->mensajes           = $CI->session->flashdata("messages");
        $sistema->mensajesExtra      = $CI->session->flashdata("messages_extra");
        
        if ($CI->session->userdata('usuario') != null) {
            $sistema->titulo             = config_item('app_name') .' | '.$modulo->alias;
            $sistema->pluginsModulo      = config_item('plugins')['modulos'][$CI->uri->segment(1)];
            $sistema->usuarioNombre      = $CI->session->userdata('usuario')->nombre;
            $sistema->usuarioImagen      = $CI->session->userdata('usuario')->imagen;
            $sistema->usuarioRol         = $CI->session->userdata('usuario')->rol;
            $sistema->encabezado         = '<span class="' . $modulo->icono . '"></span> '. $modulo->alias . $accion;
            $sistema->urlImagenPerfil    = base_url() . 'assets/img/perfil/' . $sistema->usuarioImagen;
        }
        
        return $sistema;
    }
}

if(!function_exists('actualizarConcurrencia'))
{
    function actualizarConcurrencia($idenAccion, $sistema, $usuarioId, $recursoId = null, $api = false)
    {
        $CI =& get_instance();
        $CI->load->model('model_acciones','acciones');
        $CI->load->model('model_usuarios','usuarios');
        
        $usuarioConRecurso = (($recursoId == null) ? null: $CI->usuarios->getRecurso($recursoId));
        $update = false;
        $esDisponible = true;
        //RECURSO DISPONIBLE
        if(empty($usuarioConRecurso)){
            $update = true;
            //RECURSO OCUPADO
        }else{
            //VERIFICAR SI ME PERTENECE
            if ($api) {
                $usuarioApi = $CI->_apiConfig(['methods' => ['POST'], 'requireAuthorization' => true,])['token_data'];
                if($usuarioConRecurso['id'] == $usuarioApi['id']) 
                    return true;
            } else{
                if($usuarioConRecurso['id'] == $CI->session->userdata('usuario')->id) 
                    return true;
            }

            //VERIFICAR TIEMPO
            $recurrence = date("Y-m-d H:i:s", strtotime($usuarioConRecurso['concurrencia']."+ 10 min"));
            //DISPONIBLE
            if($recurrence <  date("Y-m-d H:i:s")){
                $update = true;
                $CI->usuarios->actualizar($usuarioConRecurso['id'],['ultimorec' => null]);
            //OCUPADO
            }else{
                $time_unlock =  date("h:i A", strtotime($usuarioConRecurso['concurrencia']."+ 10 min"));
                $mensaje = "Este recurso está en uso por <b>".$usuarioConRecurso['nombre']."</b>. Será liberado por el sistema a las <b>".$time_unlock."</b> o hasta que deje de ser usado";
                if(!$api){
                    crearMensaje('warning', 'bell', 'Recurso ocupado', $mensaje);
                    $esDisponible = false;
                }else{
                    $esDisponible = false;
                }
            }
        }
        
        //ACTUALIZAR CONCURRENCIA
        if($update && $esDisponible){

            if ($api) 
                $accionId = $CI->acciones->getIdAccionActual($idenAccion, $sistema['modulo']['id']);
            else
                $accionId = $CI->acciones->getIdAccionActual($idenAccion, $sistema->modulo->id);

            if(empty($accionId)) show_error("Error al identificar la accion",404,"CONCURRENCIA|fa-exclamation-triangle");
            
            $CI->usuarios->actualizar($usuarioId,['idultimaacc' => $accionId, 'ultimorec' => $recursoId, 'concurrencia' => date('Y-m-d H:i:s')]);
        }

        return $esDisponible;
    }
}

if(!function_exists('nuevoToken'))
{
    function nuevoToken()
    {
        return md5(date('Y-m-d H:i:s').rand().rand());
    }
}

if(!function_exists('nuevoId'))
{
    function nuevoId($seed)
    {
        return md5($seed.date('Y-m-d H:i:s').rand().rand());
    }
}

if(!function_exists('generarFirma'))
{
    function generarFirma($valores)
    {
        $key = 'izimovies';
        $sep = '*';
        $end = 'BS';
        $string = '';
        foreach ($valores as $valor){
            $string .= $valor.$sep;
        }
        return md5($key.$string.$end);
    }
}

if(!function_exists('render'))
{
    function render($url, $vista = NULL)
    {
        $CI =& get_instance();
        
        $usuario = $CI->session->userdata('usuario');
        $menu['menu'] = getMenu($usuario->idrol);
        
        $datosHeader = new stdClass();
        $datosFooter = new stdClass();
        $datosHeader->mensaje = @$vista->mensaje;
     
        if ($CI->session->userdata('usuario') != null) {
            $datosFooter->estaLogueado = true;
            $datosHeader->estaLogueado = true;
        }else {
            $datosFooter->estaLogueado = false;
            $datosHeader->estaLogueado = false;
        }

        $CI->load->view('web/header', $datosHeader);
		$CI->load->view('web/menu', $menu);
		$CI->load->view($url, $vista);
		$CI->load->view('web/footer', $datosFooter);        
    }
}

if(!function_exists('renderLogin'))
{
    function renderLogin($url, $vista = NULL)
    {
        $CI =& get_instance();

        $datosHeader = new stdClass();
        $datosFooter = new stdClass();
        $datosHeader->mensaje = @$vista->mensaje;
        
        if ($CI->session->userdata('usuario') != null) {
            $datosFooter->estaLogueado = true;
            $datosHeader->estaLogueado = true;
        }else {
            $datosFooter->estaLogueado = false;
            $datosHeader->estaLogueado = false;
        }

        $CI->load->view('web/header', $datosHeader);
		$CI->load->view($url, $vista);
		$CI->load->view('web/footer');        
    }
}

if(!function_exists('enviarEmail'))
{
    function enviarEmail($urlVista, $datosVista, $datosCorreo)
    {
        $CI =& get_instance();
        //Cargamos la librería email
        $CI->load->library('email');
        
        //Establecemos la configuración de envío de emails
        $CI->email->initialize(config_item('email'));
        
        //Ponemos la dirección de correo que enviará el email y un nombre
        $CI->email->from(config_item('email')['smtp_user'], 'izimovies');

        /*
        * Ponemos el o los destinatarios para los que va el email
        * en este caso al ser un formulario de contacto te lo enviarás a ti
        * mismo
        */
        $CI->email->to($datosCorreo->correos);

        //Definimos el asunto del mensaje
        //$this->email->subject($this->input->post("asunto"));
        $CI->email->subject($datosCorreo->asunto);

        //obtenemos el cuerpo del mensaje
        $email_body = $CI->load->view($urlVista, $datosVista, true);
        
        //Definimos el mensaje a enviar
        $CI->email->message($email_body);
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if ($CI->email->send()) {
            return true;
        } else {
            return false;
        }        
    }
}

if (!function_exists('redireccionar')) 
{
    function redireccionar($helper, $clase, $esEditar = false, $id = null){
        if ($helper->estado) {
            crearMensaje('success','fas fa-check','Éxito!', $helper->mensaje);
            redirect(base_url($clase->nombre));
        } else {
            if($helper->mensaje == 'validacion'){
                if ($esEditar) {
                    $clase->editar($id);
                } else {
                    $clase->nuevo();
                }
            } else {
                crearMensaje('danger','fas fa-bell','Ups!', $helper->mensaje);
                if ($esEditar) {
                    redirect(base_url("$clase->nombre/editar/$id"));
                } else {
                    redirect(base_url("$clase->nombre/nuevo/"));
                }
            }
        }
    }
}

if (!function_exists('cargarPermisos')) 
{
    
    function cargarPermisos($sistema, $esApi = false){
    
        $CI =& get_instance();

        if ($esApi){
            $usuarioApi = $CI->_apiConfig(['methods' => ['POST'], 'requireAuthorization' => true,]);
            $permisosApi = $usuarioApi['token_data']['permisos'];
           
            foreach ($permisosApi as $i => $permiso) {
                
                $permiso = (array)$permiso;
                
                if($permiso['alias'] == $sistema['modulo']['nombre']){
                    return (object)$permiso;
                }
            }
        }
        else{
            $permisosUsuario= $CI->session->userdata('usuario')->permisos;
            
            return (object)$permisosUsuario[$sistema->modulo->nombre];
    
        }
    }
}

if (!function_exists('validarPermiso')) 
{
    
    function validarPermiso($accion, $esApi = false, $nombreModulo = ""){
        
        $CI =& get_instance();

        if ($esApi) {
            
            $usuarioApi = $CI->_apiConfig(['methods' => ['POST'], 'requireAuthorization' => true,]);
            $permisos = $usuarioApi['token_data']['permisos'];

            foreach ($permisos as $i => $permiso) {
                $permiso = (array)$permiso;
                
                if($permiso['alias'] == $nombreModulo && $permiso['acciones'][$accion] == false){
                    $response = [
                            'status'  => false,
                            'message' => "Accion no autorizada",
                            'result'  => $result
                        ];
                        $CI->api_return($response, 400);
                        die();
                }
            }

        } else {

            $permisos = $CI->session->userdata('usuario')->permisos;
            $nombreModulo = strtolower(getSistema()->modulo->nombre);

            if (!$permisos[$nombreModulo][$accion])
                return false;
            else
                return true;
        }

    }
}

if (!function_exists('paginacionIndex')) 
{
    
    function paginacionIndex($buscar = null, $sistema, $modelo){
        $CI =& get_instance();

        if ($buscar != null) 
            $sistema->busqueda = true;
        
        $CI->load->library('pagination');
        $paginacion = config_item('pagination');
        $paginacion['per_page'] = 10;
        $paginacion['base_url'] = base_url($modelo->nombreModulo.'/pagina');

        $paginacion['total_rows'] = $modelo->numeroRegistros(@$buscar);
        
        return $paginacion;
    }
}

if (!function_exists('returnApi')) 
{
    
    function returnApi($helper, $validacion = false){
        
        $CI =& get_instance();

        if ($validacion) {
            if ($helper->estado) {
                $codigoHttp = 200;
            } else {
                $codigoHttp = 400;
                $helper->mensaje = formatearErrorApi(); 
            }
        } else {
            if ($helper->estado) 
                $codigoHttp = 200;
            else 
                $codigoHttp = 400;
        }
    
    $CI->api_return($helper, $codigoHttp);

    }
}

if (!function_exists('initApi')) 
{
    function initApi(){

        $CI =& get_instance();

        header("Access-Control-Allow-Origin: *");

        $CI->_apiConfig([
            'methods' => ['POST']
        ]);
    }
}

if (!function_exists('debug')) 
{
    function debug($data){

        echo "<pre>"; print_r($data); echo "</pre>";
    }
}

if (!function_exists('verificarExistencia')) 
{
    function verificarExistencia($objeto, $esApi = false,  $mensajeApi = "El objeto solicitado no existe"){
        
        $CI =& get_instance();
        
        if(empty($objeto)){
            if ($esApi) {
                $helper = new Helper();
                $helper->estado = false;
                $helper->mensaje = $mensajeApi;

                $http_code = 400; 
                $CI->api_return($helper, $http_code); 
                die();
            } else {
                show_404();
            }
        }

    }
}

if (!function_exists('verificarPermisoExistencia')) 
{
    function verificarPermisoExistencia($sistema, $accion, $nombreClase = null, $idObjeto = null, $esComercial = false){
        
        $CI =& get_instance();

        $tienePermiso = validarPermiso($accion);

        if(!$tienePermiso)
            return false;

        if ($idObjeto != null && $nombreClase != null && $esComercial == false) {

            $CI->load->model("model_$nombreClase",$nombreClase);

            $objeto = $CI->$nombreClase->get($idObjeto);

            verificarExistencia($objeto);
        }

        return actualizarConcurrencia($accion, $sistema, $CI->session->userdata('usuario')->id, $idObjeto);

    }
}
//Funciones privadas
function getMenu($idRol){
    
    $CI =& get_instance();
    $CI->load->model('model_modulos','modulos');
    
    $menu = $CI->modulos->getMenu($idRol);

    $menuArray = [];

    if(!is_array($menu)) return [];

    foreach ($menu as $row_key => $row) {
        if(!empty($row['idmodulopad'])) continue;
        $menuArray[$row['id']] = $row;
        unset($menuArray[$row['id']]['idmodulopad']);
        unset($menuArray[$row['id']]['id']);
        unset($menuArray[$row['id']]['orden']);

        foreach ($menu as $row2_key => $row2){
            if($row2['idmodulopad'] != $row['id']) continue;
            $menuArray[$row['id']]['submenu'][$row2['id']] = $row2;
            unset($menu[$row2_key]);
            unset($menuArray[$row['id']]['submenu'][$row2['id']]['idmodulopad']);
            unset($menuArray[$row['id']]['submenu'][$row2['id']]['id']);
            unset($menuArray[$row['id']]['submenu'][$row2['id']]['orden']);
        }

        unset($menu[$row_key]);
    }

    return $menuArray;
}

function generarAccion($uri2, $uri3){
    $accion = "";
    
    if (strpos($uri2, 'uevo') !== false) {
        $accion = ' | Nuevo'; 
    } else if (strpos($uri2, 'ditar') !== false) {
        $accion = ' | Editar'; 
    }

     if($uri2 == 'pagina' and $uri3 != null)  $accion .= " | Página " . $uri3;
     elseif($uri2 == 'pagina' and $uri3 == null)  $accion .= " | Página 1";

    return $accion;
}

function formatearErrorApi(){
    return explode("<",explode("<p>",validation_errors())[1])[0];
}