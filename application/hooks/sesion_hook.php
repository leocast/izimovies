<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class sesionHook
{
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * Verifica si el token de autenticacion del usuario ha cambiado
     * si la sesion no existe, redirecciona al login.
     * si el token ha cambiado, destruye la sesion y redirecciona a
     * una vista de error con un mensaje.
     * Ademas verifica si el usuario tiene permisos para acceder al modulo
     */
    public function verificarSesion()
    {
        $uri_1 = strtolower($this->CI->uri->segment(1));
        if($uri_1 == 'sesion' || $uri_1 == 'api'|| $uri_1 == ''|| $uri_1 == 'ajax') return;

        if ($this->CI->session->has_userdata('usuario')) {
            $this->CI->load->model('model_usuarios','usuarios');
            $usuario = $this->CI->session->userdata('usuario');
            $token = $usuario->tokenaut;

            //TODO:VERIFICAR SESION DOBLE
            if($this->CI->usuarios->verificarEstadoToken($token)){
                session_destroy();
                show_error('Se inició sesión en otro equipo, navegador o teléfono','404','Sesión Doble|fa-link');
            }

            if($uri_1 != "inicio" and !$usuario->permisos[$uri_1]['ACCE']) show_404();

        } else redirect(base_url('sesion'));
    }
}