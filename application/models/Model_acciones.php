<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_acciones extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Devuelve TODAS las acciones disponibles
     * @return array|int
     */
    function getAcciones() {
        $this->db->select(["id","idmodulo","accion","identificador"]);
        $this->db->order_by("accion",'asc');

        $data = $this->db->get('acciones')->result();
        return $data;
    }

    /**
     * Regresa el ID de la accion idenfiticada por el identificador
     * y el id del modulo
     * @param $identifier
     * @param $id_module
     * @return int
     */
    function getIdAccionActual($identifier,$id_module) {
        $this->db->select("id");
        $this->db->where("idmodulo",$id_module);
        $this->db->where("identificador",strtoupper($identifier));

        $data = $this->db->get('acciones')->row_array();
        return $data['id'];
    }

    /**
     * Devuelve TRUE si el id existe en la tabla de accion
     * de lo contrario regresa FALSE
     * @param $id_action
     * @return bool
     */
    function esAccionValida($idAccion) {
        $this->db->select("id");
        $this->db->where("id", $idAccion);

        $data = $this->db->get('acciones')->num_rows();
        if($data > 0) return true;
        return false;
    }
}