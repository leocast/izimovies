<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_categorias extends CI_Model
{
    public $campos;

    public function __construct()
    {
        parent::__construct();
        $this->campos = [
            'nombre' => [
                'reglas'  => 'trim|required|min_length[3]|max_length[55]',
                'nombre'   => 'Nombre'],
        ];

    }


    public function get($id){
        $this->db->where("a.id", $id);

        $data = $this->db->get('categorias as a')->row();
        return $data;
    }

    public function getCategorias($per_page, $segment, $search = null){
        if(empty($segment)) $segment = 0; else $segment -= 1;

        $this->db->select(["a.id", "a.nombre"]);

        if($search !== null){
            $search = strtolower($search);
            $this->db->group_start();
            $this->db->like('lower(a.nombre)',$search);
            $this->db->group_end();
        }

        $this->db->order_by('a.nombre','asc');
        $data = $this->db->get('categorias as a', $per_page, ($segment * $per_page))->result();
        return $data;
    }
   
    public function numeroRegistros($search = null){
        $this->db->select("a.id");

        if($search !== null){
            $search = strtolower($search);
            $this->db->group_start();
            $this->db->like('lower(a.nombre)',$search);
            $this->db->group_end();
        }

        $data = $this->db->get('categorias as a')->num_rows();
        return $data;
    }

    public function agregar($data) {
        $this->db->trans_begin();
        $this->db->insert('categorias', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    public function actualizar($id, $data) {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('categorias', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }
    
    public function eliminar($id){
        $this->db->trans_begin();
        $this->db->where("id",$id);
        $this->db->delete('categorias');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    public function is_available($user,$id){
        $this->db->select(["id"]);
        $this->db->where("usuario",$user);
        $this->db->where("id !=",$id);

        $data = $this->db->get('usuarios')->num_rows();
        if($data > 0) return false;
        else return true;
    }

    public function getListaCategorias(){
        $this->db->select(["id","nombre"]);
        $this->db->order_by('nombre','asc');
        $data = $this->db->get('categorias')->result_array();
        return $data;
    }
}