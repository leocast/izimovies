<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_modulos extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * obtiene la información necesaria para poder crear
     * el menu en la vista, la informacion necesita ser procesada
     * solo obtiene los modulos a los que el rol tiene permiso
     * @param $id_role
     * @return array
     */
    function getMenu($idRol) {
        
        //SUBCONSULTA
        $this->db->distinct();
        $this->db->select("idmodulopad");
        $this->db->join("acciones","acciones.idmodulo = modulos.id","left");
        $this->db->join("permisos","permisos.idaccion = acciones.id","left");
        $this->db->where("permisos.idrol",$idRol);
        $this->db->where("modulos.idmodulopad IS NOT NULL");
        $this->db->from("modulos");
        $subquery = $this->db->get_compiled_select();
        //FIN SUBCONSULTA

        $this->db->reset_query();

        $this->db->distinct();
        $this->db->select(["a.id","a.nombre","a.alias","a.icono","a.idmodulopad","a.orden"]);

        $this->db->join("acciones as b","b.idmodulo = a.id","left");
        $this->db->join("permisos as c","c.idaccion = b.id","left");

        $this->db->where("a.web",true);
        $this->db->where("a.menu",true);
        $this->db->where("c.idrol",$idRol);
        $this->db->or_group_start();
        $this->db->where("a.id IN ($subquery)");
        $this->db->group_end();

        $this->db->order_by("a.orden",'asc');
        $data = $this->db->get('modulos as a')->result_array();
        return $data;
    }

    /**
     * Regresa la informacion referente a un MODULO
     * identificado por el NOMBRE
     * @param $name
     * @return array
     */
    function getModulo($nombre) {
        $this->db->select(["id","nombre","alias","icono"]);
        $this->db->where("nombre",$nombre);

        $data = $this->db->get('modulos')->row();
        return $data;
    }

    /**
     * Regresa los modulos que esten configurados para aparecer
     * en la vista para crear y modificar roles
     * @return array
     */
    function getModulos() {
        $this->db->select(["id","nombre","alias","icono"]);
        $this->db->where("modulo",true);

        $this->db->where("menu", true);

        $this->db->order_by("orden",'asc');
        $data = $this->db->get('modulos')->result();
        return $data;

    }
}