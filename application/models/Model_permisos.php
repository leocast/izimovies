<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_permisos extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Aplica un permiso a un rol, usando los ID's como identenficacdores
     *
     * @param $id_role String rol al cual se aplicara el permiso
     * @param $id_action String Accion a aplicar
     * @return int
     *
     * 1: Error
     * 2: Correcto
     */
    function setAccionesARol($idRol, $idAccion) {
        $this->db->trans_begin();
        $this->db->insert('permisos', ['idrol' => $idRol, 'idaccion'=>$idAccion]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    /**
     * Obtniene las acciones asignadas a un rol,
     * funcion utilizada en el modulo ROLES editar
     * @param $id_role
     * @return array
     */
    function getAccionesPorRol($idRol) {
        $this->db->select("idaccion");
        $this->db->where("idrol",$idRol);

        $datos = $this->db->get("permisos")->result_array();
        return $datos;
    }

    /**
     * Obtiene todos los permisos de todos los modulos asignados al rol.
     * Es necesario procesar la informacion obtenida para guardar en el
     * arreglo de session, Funcion usada para iniciar sesion y api
     *
     * @param $id_role String id del rol
     * @return array
     */
    public function getAccionesPorModulo($idRol){
        $this->db->select(['c.nombre','b.identificador']);
        $this->db->join('acciones as b','a.idaccion = b.id','inner');
        $this->db->join('modulos as c','b.idmodulo = c.id','inner');

        $this->db->where('a.idrol',$idRol);
        $datos = $this->db->get('permisos as a')->result_array();
        return $datos;
    }

    public function getAccionesConPermisosPorRol($id_rol){
        $this->db->select(["b.nombre",
            "a.accion",
            "a.identificador",
            "if(c.idrol = null, 'f', 't') as permiso"]);
        $this->db->join('modulos as b','a.idmodulo = b.id','left');
        $this->db->join('permisos as c',"a.id = c.idaccion AND c.idrol = '$id_rol'",'left');
        $this->db->order_by('a.identificador', 'asc');

        $data = $this->db->get('acciones as a')->result_array();
        return $data;
    }

    /**
     * Elimina todas las acciones asociadas a un rol
     * @param $id_role String Rol al cual borrara las acciones
     * @return int
     *
     * 1: Error
     * 0: Correcto
     */
    function eliminarAccionesDeRoles($idRol) {
        $this->db->trans_begin();
        $this->db->where("idrol", $idRol);
        $this->db->delete("permisos");

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }
}