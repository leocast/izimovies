<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_registrarPeliculas extends CI_Model
{
    public $campos;

    public function __construct()
    {
        parent::__construct();
        $this->campos = [
            'titulo' => [
                'reglas'  => 'trim|required|min_length[1]',
                'nombre'   => 'Titulo'],
            'sinopsis' => [
                'reglas'  => 'trim|min_length[0]',
                'nombre'   => 'Sinópsis'],
            'duracion' => [
                'reglas'  => 'trim|required|min_length[1]',
                'nombre'   => 'Duración'],
            'trailer' => [
                'reglas'  => 'trim|required|min_length[3]',
                'nombre'   => 'Tráiler'],
            'poster' => [
                'reglas'  => 'trim|required|min_length[1]',
                'nombre'   => 'Poster'],
            'url' => [
                'reglas'  => 'trim|required|min_length[3]',
                'nombre'   => 'URL']
        ];

    }


    public function get($id){
        $this->db->select(["a.*", "b.nombre as categoria"]);
        $this->db->join("categorias as b","b.id = a.idCategoria","left");
        $this->db->where('a.id', $id);
        $data = $this->db->get('peliculas as a')->row();

        $data->calidades = $this->getCalidadesByIdPelicula($data->id);

        return $data;
    }

    public function getAll($per_page, $segment, $search = null){
        
        $this->db->select(["a.*", "b.nombre as categoria"]);
        $this->db->join("categorias as b","b.id = a.idCategoria","left");

        if(empty($segment)) $segment = 0; else $segment -= 1;

        if($search !== null){
            $search = strtolower($search);
            $this->db->group_start();
            $this->db->like('lower(a.titulo)',$search);
            $this->db->group_end();
        }

        $this->db->order_by('a.titulo','asc');
        $data = $this->db->get('peliculas as a', $per_page, ($segment * $per_page))->result_array();
        
        foreach ($data as $i => $dato) 
            $data[$i]['calidades'] = $this->getCalidadesByIdPelicula($dato['id']);

        return $data;
    }
   
    public function numeroRegistros($search = null){
        $this->db->select("a.id");

        if($search !== null){
            $search = strtolower($search);
            $this->db->group_start();
            $this->db->like('lower(a.titulo)',$search);
            $this->db->group_end();
        }

        $data = $this->db->get('peliculas as a')->num_rows();
        return $data;
    }

    public function agregar($data) {
        $this->db->trans_begin();
        $this->db->insert('peliculas', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    
    public function agregarCalidadesPeliculas($data) {
        $this->db->trans_begin();
        $this->db->insert('calidadespeliculas', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    public function actualizar($id, $data) {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('peliculas', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }
    
    public function eliminar($id){
        $this->db->trans_begin();
        $this->db->where("id",$id);
        $this->db->delete('peliculas');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    public function is_available($user,$id){
        $this->db->select(["id"]);
        $this->db->where("usuario",$user);
        $this->db->where("id !=",$id);

        $data = $this->db->get('usuarios')->num_rows();
        if($data > 0) return false;
        else return true;
    }

    public function getCalidadesByIdPelicula($idPelicula){
        
        $this->db->select(["a.nombre", "a.subfijo", "a.id"]);
        $this->db->join("calidadespeliculas as b","b.idCalidad = a.id","left");
        $this->db->where("b.idPelicula", $idPelicula);
        $data = $this->db->get('calidades as a')->result();
        return $data;
    }

    public function getListaCalidades(){
        $this->db->select(["a.id", "a.nombre", "a.subfijo"]);
        $data = $this->db->get('calidades as a')->result_array();
        return $data;
    }

    public function getTopId()
    {
        $query = "SELECT id FROM peliculas
        order by id desc LIMIT 1";

        $data = $this->db->query($query)->row();
        return $data;
    }

    public function eliminarCalidadesPelicula($id){
        $this->db->trans_begin();
        $this->db->where("idPelicula",$id);
        $this->db->delete('calidadespeliculas');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }
}