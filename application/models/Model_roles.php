<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_roles extends CI_Model
{
    public $fields;

    public function __construct()
    {
        parent::__construct();
        $this->campos = [
            'nombre' => [
                'reglas'  => 'trim|required|min_length[3]|max_length[32]|alpha_numeric_spaces',
                'unique'  => '|is_unique[roles.nombre]',
                'formato' => 'ucwords',
                'nombre'  => 'Nombre'],
        ];
    }

    /**
     * Obtiene solo un rol de la base de datos
     * identificado por el id_rol
     * @param $id_role
     * @return array|int
     */
    public function get($idRol){
        $this->db->select(["id","nombre","activo"]);
        $this->db->where('id',$idRol);

        $data = $this->db->get('roles')->row();
        return $data;
    }


    /**
     * Agrega un nuevo rol, recibe un arreglo asociativo con los campos de la BD
     * @param $data [] [id,nombre,descripcion,activo,creacion]
     * @return int
     */
    public function agregar($datos) {
        $this->db->trans_begin();
        $this->db->insert('roles', $datos);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }


    /**
     * Recibe el id del rol a modificar, seguido de un arreglo
     * asociativo con los campos a moficiar
     * @param $id
     * @param $data
     * @return int
     *
     */
    public function actualizar($id, $data) {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('roles', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    /**
     * Obtiene todos los roles con su informacion para mostrar en index
     * @param $per_page Int Cuantos roles mostrara por pagina
     * @param $segment Int Numero de pagina en la que se encuentra
     * @param null $search Palabra clave para filtrar los roles
     * @return array|bool
     */
    public function getRoles($porPagina, $segmento, $busqueda = null){
        if(empty($segmento)) $segmento = 0; else $segmento -= 1;
        if($busqueda !== null){
            $busqueda = strtolower($busqueda);
            $this->db->group_start();
            $this->db->like('lower(nombre)',$busqueda);
            $this->db->group_end();
        }
        $this->db->order_by('nombre','asc');
        $data = $this->db->get('roles', $porPagina, ($segmento * $porPagina))->result();
        return $data;
    }

    /**
     * Obtiene los roles activos
     * necesario para los select list
     * @return array
     */
    public function getListaRoles(){
        $this->db->select(["id","nombre"]);
        $this->db->where('activo',true);
        $this->db->order_by('nombre','asc');
        $data = $this->db->get('roles')->result_array();
        return $data;
    }

    /**
     * Obtiene el total filas a los roles con o sin busqueda
     * utilizada principalmente para la paginacion
     * @param null $search Palabra clave para filtrar la informacion
     * @return int
     * Int: Total de filas encontradas
     */
    public function numeroRegistros($buscar = null){
        if($buscar !== null){
            $buscar = strtolower($buscar);
            $this->db->group_start();
            $this->db->like('lower(nombre)',$buscar);
            $this->db->group_end();
        }

        $data = $this->db->get('roles')->num_rows();
        return $data;
    }

    /**
     * Regresa TRUE si el NOMBRE del rol esta disponible
     * o si pertenece a si mismo,
     * de lo contrario regresa FALSE
     * @param $name
     * @param $id
     * @return bool
     */
    public function is_available($name,$id){
        $this->db->select(["id"]);
        $this->db->where("nombre",$name);
        $this->db->where("id !=",$id);

        $data = $this->db->get('roles')->num_rows();
        if($data > 0) return false;
        else return true;
    }

    /**
     * Elimina un Rol identificado por el ID
     * @param $id_role
     * @return int
     */
    public function eliminar($idRol){
        $this->db->trans_begin();
        $this->db->where("id", $idRol);
        $this->db->delete('roles');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    /**
     * Regresa TRUE si el rol contiene movimientos
     * de lo contrario regresa FALSE
     * @param $id_role
     * @return bool
     */
    public function tieneDependencias($idRol) {
        $this->db->select(["a.id"]);
        $this->db->join("usuarios as b","b.idrol = a.id","left");
        $this->db->where("b.idrol", $idRol);

        $data = $this->db->get('roles as a')->num_rows();
        if ($data > 0) return true;
        else return false;
    }
}