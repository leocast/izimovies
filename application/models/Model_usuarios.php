<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class model_usuarios extends CI_Model
{
    public $camposUsuarios;
    public $camposSesion;

    public function __construct()
    {
        parent::__construct();
        $this->camposUsuarios = [
            'usuario' => [
                'reglas'  => 'trim|required|min_length[3]|max_length[16]|alpha_numeric',
                'unique' => '|is_unique[usuarios.usuario]',
                'formato' => 'strtolower',
                'nombre'   => 'Usuario'],

            'contrasena' => [
                'reglas'   =>'trim|required|min_length[6]|max_length[20]',
                'formato'  => '',
                'nombre'    => 'Contraseña'],

            'ccontrasena' => [
                'reglas'   =>'trim|matches[contrasena]',
                'formato'  => '',
                'nombre'    => 'Confirmar Contraseña'],

            'idRol' => [
                'reglas'   =>'trim|required',
                'formato'  => '',
                'nombre'    => 'Rol'],

            'nombre' => [
                'reglas'   =>'trim|required|min_length[3]|max_length[64]',
                'formato'  => 'ucwords',
                'nombre'    => 'Nombre'],

            'correo' => [
                'reglas'   =>'trim|required|valid_email',
                'formato'  => 'strtolower',
                'nombre'    => 'Correo'],
        ];

        $this->camposSesion = [
            'usuario' => [
                'reglas'  => 'trim|required',
                'nombre'   => 'Usuario'],
            'contrasena' => [
                'reglas'  => 'trim|required',
                'nombre'   => 'Contraseña'],
        ];
    }

    /**
     * Identifica si usuario y contraseña existen en la base de datos
     * de ser asi, regresa la informacion necesaria para la variable sesion del usuario
     * @param $user
     * @param $pass
     * @return array
     */
    public function login($usuario, $contrasena) {
        $this->db->select(["a.*", "b.nombre as rol"]);

        $this->db->join('roles as b','a.idrol = b.id','left');

        $this->db->where("a.usuario", $usuario);
        $this->db->where("a.contrasena", $contrasena);
        $this->db->where("a.activo", true);

        $data = $this->db->get('usuarios as a')->row();
        return $data;
    }

    /**
     * Regresa la información correspondiente a un usuario
     * @param $id_user
     * @return array
     */
    public function get($id_user){
        $this->db->select(["a.*", "b.nombre as rol"]);
        $this->db->join('roles as b','a.idrol = b.id','inner');
        $this->db->where("a.id", $id_user);

        $data = $this->db->get('usuarios as a')->row();
        return $data;
    }

    /**
     * Obtiene el nombre del usuario
     * @param $id_user
     * @return array
     */
    public function get_name_by_id($id_user){
        $this->db->select("cnombre");
        $this->db->where("cid", $id_user);

        $data = $this->db->get('usuarios')->row_array();
        return $data['cnombre'];
    }

    /**
     * Regresa la informacion de concurrencia del usuario
     * y ultimos accesos y creacion
     * especificado
     * @param $id_user
     * @return array
     */
    public function getInformacionAcceso($id_user){
        $this->db->select(["a.nombre","a.creacion","a.plataforma","a.concurrencia","a.ultimoacc","b.accion","c.alias"]);
        $this->db->join('acciones as b','a.idultimaacc = b.id','left');
        $this->db->join('modulos as c','b.idmodulo = c.id','left');
        $this->db->where("a.id", $id_user);

        $data = $this->db->get('usuarios as a')->row();
        return $data;
    }

    /**
     * Regresa todos los usuarios de la DB, filtrados por busqueda
     * usado para index
     * @param $per_page
     * @param $segment
     * @param null $search
     * @return array
     */
    public function getUsuarios($per_page, $segment, $search = null){
        if(empty($segment)) $segment = 0; else $segment -= 1;

        $this->db->select(["a.id",
            "a.nombre",
            "a.usuario",
            "a.creacion",
            "a.activo",
            "a.correo",
            "b.nombre as rol"]);
        $this->db->join('roles as b','a.idrol = b.id','inner');

        if($search !== null){
            $search = strtolower($search);
            $this->db->group_start();
            $this->db->like('lower(a.nombre)',$search);
            $this->db->or_like('lower(a.usuario)',$search);
            $this->db->group_end();
        }

        $this->db->order_by('a.usuario','asc');
        $data = $this->db->get('usuarios as a', $per_page, ($segment * $per_page))->result();
        return $data;
    }

    /**
     * Obtiene el total filas a los roles con o sin busqueda
     * utilizada principalmente para la paginacion
     * @param null $search
     * @return int
     */
    public function numeroRegistros($search = null){
        $this->db->select("a.id");
        $this->db->join('roles as b','a.idrol = b.id','inner');

        if($search !== null){
            $search = strtolower($search);
            $this->db->group_start();
            $this->db->like('lower(a.nombre)',$search);
            $this->db->or_like('lower(a.usuario)',$search);
            $this->db->group_end();
        }

        $data = $this->db->get('usuarios as a')->num_rows();
        return $data;
    }

    /**
     * recibe un arreglo asociativo
     * con el nombre del campo y valor a insertar
     * @param $data
     * @return int
     */
    public function agregar($data) {
        $this->db->trans_begin();
        $this->db->insert('usuarios', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    /**
     * recibe el id del usuario a modificar, seguido de un arreglo asociativo
     * con el nombre del campo y valor a modificar
     * @param $id
     * @param $data
     * @return int
     */
    public function actualizar($id, $data) {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('usuarios', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    /**
     * verifica si el token del usuario ha cambiado
     * regresa TRUE si cambió, FALSE si no
     * @param $token
     * @return bool
     */
    public function verificarEstadoToken($token) {
        $this->db->select(["id"]);
        $this->db->where("tokenaut", $token);

        $data = $this->db->get('usuarios')->num_rows();
        if ($data > 0)  return true;
        else return false;
    }

    /**
     * Regresa TRUE si el id de usuario es el ultimo usuario tipo Administrador
     * de lo contrario regresa FALSE
     * @param $id_user
     * @return bool
     */
    public function esUltimoAdministrador($id_user) {
        $this->db->select(["id"]);
        $this->db->where("idrol", config_item('idRolAdmin'));
        $this->db->where("activo", true);
        $this->db->where("id !=", $id_user);

        $data = $this->db->get('usuarios')->num_rows();
        if ($data > 0) return false;
        else return true;
    }

    /**
     * Regresa la informacion revelante del usuario
     * quien está utilizando un recurso
     * @param $id_resource
     * @return array
     */
    public function getRecurso($idRecurso) {
        $this->db->select(["id","nombre","concurrencia"]);
        $this->db->where("ultimorec", $idRecurso);

        $data = $this->db->get('usuarios')->row_array();
        return $data;
    }

    /**
     * Regresa TRUE si el USUARIO del usuario esta disponible
     * o pertenece a si mismo,
     * de lo contrario regresa FALSE
     * @param $user
     * @param $id
     * @return bool
     */
    public function is_available($user,$id){
        $this->db->select(["id"]);
        $this->db->where("usuario",$user);
        $this->db->where("id !=",$id);

        $data = $this->db->get('usuarios')->num_rows();
        if($data > 0) return false;
        else return true;
    }

    /**
     * @param $id_user
     * @return int
     */
    public function eliminar($id_user){
        $this->db->trans_begin();
        $this->db->where("id",$id_user);
        $this->db->delete('usuarios');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 1;
        } else {
            $this->db->trans_commit();
            return 0;
        }
    }

    /**
     * Regresa el id y nombre del usuario al que pertenece el email
     * Se usa para recuperacion de contraseña
     * @param $email
     * @return array
     */
    public function getUsuarioPorEmail($email){
        $this->db->select(["id","nombre"]);
        $this->db->where("activo", true);
        $this->db->where("correo", $email);

        $data = $this->db->get('usuarios')->row();
        return $data;
    }

    /**
     * Regresa el id y nombre del usuario al que pertenece el token de recuperacion
     * Se usa para recuperacion de contraseña
     * @param $email
     * @return array
     */
    public function getUsuarioPorTokenRec($tokenRecuperacion){
        $this->db->select(["id","nombre"]);
        $this->db->where("tokenrec is not null");
        $this->db->where("tokenrec", $tokenRecuperacion);

        $data = $this->db->get('usuarios')->row();
        return $data;
    }

}