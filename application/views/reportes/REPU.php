<body>

<?= form_open(base_url() . 'reportes/generar')?>

<input type="hidden" name="tipo" id="REPU_XLS" value="REPU_XLS">

	<div class="card" style="width: 80%; margin: auto;">
		<div class="card-header text-center">
            <div class="card-title" style="float: none;">
            <div class="row icon_tittle_report">
                <div class="col-sm-12">
                    <h1 style="margin: auto; color:white;"><i class="fas fa-dolly" style="float: left!important;"></i></h1>
                    <h2 style="margin: auto; color:white; float: left; margin-left:5px;" id="total_label"> </h2>
                </div><br>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="tittle_report" style="color:white; margin: auto;"> Reporte de prueba de usuarios</h1>
                </div>
            </div><br>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="tittle_report" style="color:white; margin: auto;"> Del <?= $parametros->fechaInicio . " al " . $parametros->fechaFin ?></h3>
                </div>
            </div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-12">

                    <table id="orders-grid"  class="table-responsive table table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th data-order="asc" data-column-id="id" data-type="string">#</th>
                            <th data-column-id="nombre" data-type="string">Nombre</th>
                            <th data-column-id="usuario" data-type="string">Usuario</th>
                            <th data-column-id="creacion" data-type="date">Creacion</th>
                            <th data-column-id="activo" data-type="string">Activo</th>
                            <th data-column-id="correo" data-type="string">Correo</th>
                            <th data-column-id="rol" data-type="string">Rol</th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php 
                                foreach ($datosReporte as $row) {
                                echo "<tr>".
                                        "<td>" . $row->id           . "</td>" .
                                        "<td>" . $row->nombre       . "</td>" .
                                        "<td>" . $row->usuario      . "</td>" .
                                        "<div>".
                                            "<td>" . $row->creacion . "</td>" .
                                        "</div>".
                                        "<td>" . $row->activo       . "</td>" .
                                        "<td>" . $row->correo       . "</td>" .
                                        "<td>" . $row->rol          . "</td>" .
                                    "</tr>";
                                }
                            ?>
                    </tbody>
                    </table>

				</div>
			</div>
		</div>
	</div>
</body>

</html>
