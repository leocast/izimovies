<body>
	<!-- Start cards container -->
	<div class="container-rows" id="acordeon">
		<!-- Start Card -->
		<div class="row-wrap">
			<div class="hvr-grow-shadow-reports" id="headingOne">
				<div class="row-background">
					<div class="rowNew bg-white">
						<div class="col-lg-12 start-flex">
							<div class="number-square">
								<div class="numeral">#</div> 1
							</div>
							<div class="nextSquare">
								<div class="text-tittle"><i class="fas fa-dolly"></i>&nbsp; Reporte de prueba</div>
								<div class="buttonFlex">
									<button type="button" data-toggle="collapse" aria-controls="collapseOne" data-target="#collapseOne" class="btn btn-primary btn-block rightButton btn-rounded"><i
										 class="fas fa-arrow-down"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="collapse-container">
			<div class="collapse" id="collapseOne" data-parent="#acordeon">
				<div class="card" style="width: 100%; margin: 0 auto;">
					<div class="card-header">
						<div class="card-title ">Parámetros del Reporte</div>
					</div>
					<div class="card-body">
							<?= form_open(base_url() . 'reportes/generar', 'class="col-sm-12"')?>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="id_fecha_inicio">Parámetro 1<span class="text-danger">*</span></label>
										<input id="id_fecha_inicio" autocomplete="off" type="text" class="form-control" name="fechaInicio" value=""
										placeholder="Parámetro 1">
										<span class="form-text">Parámetro de prueba 1</span>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="id_fecha_fin">Parámetro 2<span class="text-danger">*</span></label>
										<input id="id_fecha_fin" autocomplete="off" type="text" class="form-control" name="fechaFin" value=""
										placeholder="Párametro 2">
										<span class="form-text">Parámetro de prueba 2</span>
									</div>
								</div>
						
								<div class="col-sm-12">
									<button class="btn btn-primary btn-block" id="btnGenerar">
										<i class="fas fa-file-download"></i> GENERAR REPORTE
									</button>
                                    <input type="hidden" name="tipo" value="REPU">
								</div>
							</div>
							<?= form_close(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- End Card -->

	<!-- End div container -->
    </div>
</body>
</html>
