<?= form_open(base_url($urlAccion)) ?>

<div style="display: flex;" class="justify-content-center"> 
<div class="flexy70">
<div class="row">
        <div style="margin-bottom:20px" class="col-12 m-t-3">
            <center>
                <button type="submit" class="btn todo-rosa">
                    <span class="fas fa-save"></span> Guardar
                </button>
            </center>
        </div>
    </div>
<div style="margin-bottom:20px" class="card-izi card">
    <div class="todo-rosa card-header">
        <div class="card-title">Información Del Rol</div>
    </div>
    <div class="text-left card-body">
        <div class="row">
            <div class="col-sm-9">
                <div class="form-group">
                    <label style="font-size: 1em!important" for="id_rol">Nombre<span class="text-danger">*</span></label>
                    <input id="id_rol" type="text" class="form-control txtRosa" name="nombre" value="<?= set_value('nombre',@$nombre) ?>" placeholder="Nombre">
                  
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label style="font-size: 1em!important" for="id_activo">Rol Activo</label> <br>
                    <label class="switch">
                        <input type="checkbox" id="id_activo"  name="activo" <?=$active?> >
                        <span></span>
                    </label>
                    <span class="form-text">Estado actual del rol</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php foreach ($modulos AS $modulo): ?>
        <div style="margin-bottom:20px" class="col-sm-4 col-md-3 ">
            <div class="card-izi card ">
                <div class="todo-rosa card-header">
                    <div class="card-title"><span class="<?= $modulo->icono ?>"></span> <?= $modulo->alias ?></div>
                </div>
                <div class="card-body padding-10">
                    <?php if(isset($acciones) and in_array($modulo->id, array_column($acciones, 'idmodulo')) and !empty($acciones)): ?>
                        <?php foreach ($acciones as $row_acciones => $accion): ?>
                            <?php if($accion->idmodulo == $modulo->id): ?>
                                <div class="form-group form-inline switch-custom margin-bottom-5">
                                    <label class="switch switch-sm m-0">
                                        <input type="checkbox"
                                               id="accion_ident-<?=$accion->idmodulo.'-'.$accion->identificador?>"
                                               name="accion_ident-<?=$accion->id?>"
                                            <?=(key_exists($accion->id,$actives_checks))?'checked':''?>>
                                        <span></span>
                                    </label> &nbsp;
                                    <div>
                                        <label for="accion_ident-<?=$accion->idmodulo.'-'.$accion->identificador?>">
                                            <?= $accion->accion?>
                                        </label>
                                    </div>
                                </div>
                                <?php unset($acciones[$row_acciones]); ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <div align="center">
                        <span class="fas fa-info-circle fa-4x text-primary"></span> <br>
                        No hay <strong>Acciones</strong> disponibles
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
</div>
</div>
<?= form_close() ?>

