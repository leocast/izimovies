<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="display: flex;" class="justify-content-center"> 
<div class="flexy50">
<?php if($permisos->CREA):?>
<div class="row">
        <div class="col-12 m-t-2 m-b-3">
            <button onclick="Modulo.funciones.accion('nuevo');" style="float: right; margin-bottom:20px;" type="submit" class="btn todo-rosa">
                <span class="fas fa-plus-circle"></span> Nuevo
            </button>
        </div>
    </div>
<?php endif;?>
<div style="border:none" class="card">
    <div class="table-responsive">
    <?php if(!empty($roles)): ?>
            <table id="dt-table" class="table table-striped table-hover margin-bottom-0" cellspacing="0" width="100%">
                <thead class="thead-dark">
                <tr>
                    <th><span class="fas fa-search text-info"></span> Rol</th>
                    <th>Creación</th>
                    <th class="text-center">Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($roles as $i => $rol): ?>
                    <tr>
                        <td>
                            <div class="mt-1">
                                <?= $rol->nombre ?>
                            </div>
                        </td>
                        <td>
                            <div class="mt-1">
                                <?= $rol->creacion ?>
                        </td>
                        <td class="text-center">
                            <div class="mt-1">
                            <?php if($rol->activo == true):?>
                                    <span class="fas fa-check-square text-success fa-2x"></span>
                                <?php else:?>
                                    <span class="fas fa-minus-square text-danger fa-2x"></span>
                            <?php endif;?>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group" role="group">
                                <!-- Editar -->
                                <?php if($permisos->MODI):?>
                                    <button onclick="Modulo.funciones.accion('editar/<?=$rol->id?>');" class="btn todo-rosa btn-icon btn-sm"><span class="fas fa-pencil-alt"></span></button>
                                <?php else:?>
                                    <button class="btn btn-secondary btn-icon btn-sm"><span class="fas fa-pencil-alt"></span></button>
                                <?php endif;?>
                                
                                <!-- Eliminar -->
                                <?php if($permisos->ELIM):?>
                                    <button onclick="Modulo.funciones.eliminar(event,'<?=$rol->id?>');" class="btn btn-dark btn-icon btn-sm"><span class="fas fa-trash-alt"></span></button>
                                <?php else:?>
                                    <button class="btn btn-secondary btn-icon btn-sm"><span class="fas fa-trash-alt"></span></button>
                                <?php endif;?>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <div class="text-center padding-20" style="margin: 15px;">
                <span style="color:#CE2E48" class="fas fa-info-circle fa-4x"></span> <br>
                No hay <strong>Roles</strong> disponibles
                <?php if(isset($buscar) and !empty($buscar)): ?>
                    relacionados con <b><?= $buscar ?></b>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div style="margin-top: 20px;"class="todo-rosa card">
    <div class="card-body ">
        <div class="row">
        <div class="col-md-6 text-truncate col-sm-6 padding-top-5 text-xs-center text-md-left margin-xs-bottom-5">
        <div class="margin-t-5 ">
            Mostrando <b><?= $actualReg ?></b> de <b><?= $totalReg ?></b> Registros
        </div>        
            </div>
            <div class="col-md-6 col-sm-12 text-md-right text-sm-center text-xs-center margin-xs-bottom-5 margin-b-14">
                <div class="row flex-end">
                    <div class="pagination-fix">
                        <?= $this->pagination->create_links() ?>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(isset($buscar) and !empty($buscar)): ?>
<div class="row">
    <div class="col-12 m-t-10 m-b-3">
        <a href="<?= base_url() ?>roles" class="btn todo-rosa m-0 btn-sm"><i class="fas fa-eraser"></i> Limpiar</a>
    </div>
</div>
<?php endif; ?>
</div>
</div>