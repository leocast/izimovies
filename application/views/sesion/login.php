    <body class="fixed-left">
        <div class="messages-div">
           
            <?= (isset($mensaje))? $mensaje: ''?>
            <?php if(validation_errors()): ?>
                <div class='alert alert-danger alert-dismissible fade show shadow'>
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                    <h4 class='alert-heading'><span class='fas fa-bell'></span> Error de Validación</h4>
                    
                </div>
            <?php endif; ?>
        </div>

        <!-- Begin page -->
        <div class="backBlack"></div>
        <div class="accountbg"></div>
        <div class="wrapper-page" style="margin-top: 220px">

            <div style="z-index:2!important; background-color: rgb(250,250,250,.85);" class="card z2">
                <div class="card-body">

                    <!-- <h3 class="text-center m-0">
                        <a href="index.html" class="logo logo-admin"><img src="<?=base_url();?>assets/images/app/login.png" height="115" alt="logo"></a>
                    </h3> -->

                    <div class="p-3">
                        <h1 style="color: rgb(206, 46, 72)!important" class="text-muted m-b-5 text-center">izi<strong>Movies</strong></h1>
                        <p style="color: rgb(206, 46, 72)!important" class="text-muted text-center">Encuentra tu pelicula izi</p>

                        <?= form_open( base_url('sesion/login'),['autocomplete'=>'off']) ?>

                            <div class="form-group">
                                <!-- <label for="username">Usuario</label> -->
                                <input style="color: rgb(206, 46, 72);" type="text" class="text-rosa form-control" id="username" placeholder="Usuario" name="usuario">
                            </div>

                            <div class="form-group">
                                <!-- <label for="userpassword">Contraseña</label> -->
                                <input style="color: rgb(206, 46, 72);" type="password" class="text-rosa form-control" id="userpassword" placeholder="Contraseña" name="contrasena">
                            </div>

                            <div class="form-group row m-t-20">
                                <div style="display:flex; justify-content:flex-end;" class="col-sm-12 text-right">
                                    <button style="background-color: rgb(206, 46, 72);border-color: rgb(206, 46, 72);" class="cargarLoader btn btn-primary w-md waves-effect waves-light" type="submit">Iniciar Sesión</button><br>
                                </div>
                            
                            </div>
                            <hr>
                            <div class="form-group mb-0 row">
                                <div class="col-12 m-t-3">
                                    <a style="color: rgb(206, 46, 72);" href="<?=base_url();?>sesion/olvidoContrasena" class="cargarLoader"><i class="mdi mdi-lock"></i> Olvidaste tu contraseña?</a>
                                    <br><a style="color: rgb(206, 46, 72);" href="<?=base_url();?>sesion/registrarse" class="cargarLoader"><i class="fas fa-user-plus"></i> Regístrate!</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p style="z-index: 20!important; color:white"class="text-white">© 2019 Developed with <i class="mdi mdi-heart text-danger"></i> by Leo</p>
            </div>
        </div>

<script type='text/javascript' src='/tortilleria/assets/js/jquery.min.js'></script>
<script type='text/javascript' src='/tortilleria/assets/plugins/select2/js/select2.min.js'></script>
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/sweetalert2@8'></script>
<script type='text/javascript' src='/tortilleria/assets/js/app/init.js'></script>
<script type='text/javascript' src='/tortilleria/assets/js/app/init_sesion.js'></script>