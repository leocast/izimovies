<body class="fixed-left">
<div class="messages-div">
    <?= (isset($mensaje))? $mensaje: ''?>
    <?php if(validation_errors()): ?>
        <div class='alert alert-danger alert-dismissible fade show shadow'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
            <h4 class='alert-heading'><span class='fas fa-bell'></span> Error de Validación</h4>
            <?=validation_errors() ?>
        </div>
    <?php endif; ?>
</div>

<!-- Begin page -->
<div class="backBlack"></div>
<div class="accountbg"></div>
<div style="z-index:2!important;  max-width: 1000px!important; margin-top: 300px!important;"class="wrapper-page">
            
<?= form_open_multipart(base_url("sesion/registrarNuevoUsuario")) ?>
<!-- <div style="background-color: rgb(250,250,250,.85);" class="card"> -->
    <!-- <div class="card-body padding-5 card-button"> -->
        <!-- <div class="row"> -->
            <!-- <div class="col-12 card-title"> -->
                <!-- <button type="submit" class="btn todo-rosa">
                    <span class="fas fa-save"></span> GUARDAR
                </button> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->
<!-- </div> -->

<div class="row">
    <div class="col-md-3">
        <div style="border: none!important; background-color: rgb(250,250,250,0);" class="card sticky">
            <!-- <div class="card-header">
                <div class="card-title">Imagen De Perfil</div>
            </div> -->
            <div class="card-body ">
                <div class="row">
                    <div style="margin-top: -26px;" class="col-12">
                        <div class="perfil-wrapper">
                            <img style=" border-radius: 10px;
    margin-bottom: 20px; margin-top:45x" src="<?= base_url("assets/images/app/perfilRegistro.jpg") ?>" class="view-image-perfil"  id="id_perfil_image"/>
                        </div>

                        <div class="form-group margin-0">
                            <span class="btn btn-file btn-block perfil-button todo-rosa">
                                <span class="fas fa-image"></span> Perfil <input type="file" id="urlImagen" accept="image/*" name="imagen">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div style="background-color: rgb(250,250,250,.95);border: none!important;" class="card ">
                    <!-- <div class="card-header">
                        <div class="card-title">Información General</div>
                    </div> -->
                    <div class="card-body">
                        <div class="row">
                        
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="id_nombre">Nombre<span class="text-danger"> *</span></label>
                                    <input id="id_nombre" type="text" class="text-rosa form-control" name="nombre" value="<?= set_value('nombre',@$nombre) ?>" placeholder="Nombre">
                                    <!-- <span class="form-text etiqueta-helper">Nombre del usuario</span> -->
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="id_correo">Correo<span class="text-danger"> *</span></label>
                                    <input id="id_correo" type="text" class="text-rosa form-control" name="correo" value="<?= set_value('correo',@$correo) ?>" placeholder="Correo Electrónico">
                                    <!-- <span class="form-text etiqueta-helper">Indica el correo electrónico</span> -->
                                </div>
                            </div>

                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="id_usuario">Usuario<span class="text-danger"> *</span></label>
                                    <input id="id_usuario" type="text" class="text-rosa form-control" name="usuario" value="<?= set_value('usuario',@$usuario) ?>" placeholder="Usuario">
                                    <!-- <span class="form-text etiqueta-helper">Usuario para acceder al sistema</span> -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="id_contrasena">Contraseña<span class="text-danger"> *</span></label>
                                    <input id="id_contrasena" type="password" class="text-rosa form-control" name="contrasena" value="<?= set_value('contrasena',@$contrasena) ?>" placeholder="Contraseña">
                                    <!-- <span class="form-text etiqueta-helper">Contraseña de acceso</span> -->
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="id_ccontrasena">Confirmar Contraseña<span class="text-danger"> *</span></label>
                                    <input id="id_ccontrasena" type="password" class="text-rosa form-control" name="ccontrasena" value="<?= set_value('cccontrasena',@$cccontrasena) ?>" placeholder="Confirmar">
                                    <!-- <span class="form-text etiqueta-helper">Confirmar contraseña</span> -->
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="    display: flex;
    width: 100%;
    justify-content: flex-end;
    margin-right: 16px;
    margin-top: -50px;
    z-index: 3">
        <button type="submit" class="btn todo-rosa">
            <span class="fas fa-"></span> Registrarse
        </button>
    </div>
</div>
<?= form_close() ?>


</div>

<script type='text/javascript' src='../assets/js/jquery.min.js'></script>
<script type='text/javascript' src='../assets/plugins/select2/js/select2.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/sweetalert2@8'></script>
<script type='text/javascript' src='../assets/js/app/init.js'></script>
<script type='text/javascript' src='../assets/js/app/init_usuarios.js'></script>

