<body class="fixed-left">
        <div class="messages-div">
            <?= (isset($mensaje))? $mensaje: ''?>
        </div>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0">
                        <a href="index.html" class="logo logo-admin"><img src="<?=base_url();?>assets/images/app/login.png" height="115" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Reestablecer contraseña</h4>
                        <p class="text-muted text-center">Ingrese su dirección de correo electrónico y le enviaremos un enlace para restablecer su contraseña.</p>

                        <?= form_open( base_url('sesion/olvidoContrasenaProces'),['autocomplete'=>'off']) ?>

                            <div class="form-group">
                                <label for="username">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Ingrese su correo" name="email">
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-sm-6">
                                    <!-- <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                                    </div> -->
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Enviar</button>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="<?=base_url();?>sesion" class="text-muted"><i class="mdi mdi-lock    "></i> Iniciar Sesión</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="text-white">© 2019 Developed with <i class="mdi mdi-heart text-danger"></i> by Genius Code</p>
            </div>
        </div>
