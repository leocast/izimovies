<body class="fixed-left">
    <div class="messages-div">
            <?= (isset($mensaje))? $mensaje: ''?>
            <?php if(validation_errors()): ?>
                <div class='alert alert-danger alert-dismissible fade show shadow'>
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                    <h4 class='alert-heading'><span class='fas fa-bell'></span> Error de Validación</h4>
                    <?=validation_errors() ?>
                </div>
            <?php endif; ?>
        </div>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0">
                        <a href="index.html" class="logo logo-admin"><img src="<?=base_url();?>assets/images/app/login.png" height="115" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Hola <?=@$nombre?>.</h4>
                        <p class="text-muted text-center">Recibimos su solicitud para reestablecer la contraseña, a continuación escriba su nueva clave de acceso.</p>

                        <?= form_open( base_url('sesion/reestablecerContrasenaC'),['autocomplete'=>'off']) ?>
                            <input type="text" hidden name="tokenRec" value="<?=$tokenRec?>">
                            <div class="form-group">
                                <label for="username">Nueva contraseña</label>
                                <input type="password" class="form-control" id="email" placeholder="Ingrese su contraseña nueva" name="contrasena">
                            </div>

                            <div class="form-group">
                                <label for="username">Confirmar contraseña</label>
                                <input type="password" class="form-control" id="email" placeholder="Confirme su contraseña" name="ccontrasena">
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-sm-6">
                                    <!-- <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                                    </div> -->
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Reestablecer</button>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="<?=base_url();?>sesion" class="text-muted"><i class="mdi mdi-lock    "></i> Iniciar Sesión</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="text-white">© 2019 Developed with <i class="mdi mdi-heart text-danger"></i> by Genius Code</p>
            </div>
        </div>
