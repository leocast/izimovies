
<?= form_open_multipart(base_url($urlAccion)) ?>

<div style="display: flex;" class="justify-content-center"> 
<div class="flexy70">
<input type="hidden" name="id" value="<?= set_value('id',@$id) ?>">

<div class="row">
    <div class="col-md-2">
        <div style="width:220px; float:right" class="card-izi card sticky">
            <div class="todo-rosa card-header">
                <div class="card-title">Imagen de perfil</div>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-12">
                        <div class="perfil-wrapper">
                            <img style="width:200px; height: 197px" src="<?= base_url("assets/img/perfil/$imagenPerfil") ?>" class="view-image-perfil"  id="id_perfil_image"/>
                        </div>

                        <div class="form-group margin-0">
                            <span class="todo-rosa btn  btn-file btn-block perfil-button">
                                <span class="fas fa-image"></span> Imagen <input type="file" id="urlImagen" accept="image/*" name="imagen">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-left col-md-10">
        <div class="row">
            <div class="col-md-12">
                <div class="card-izi  card ">
                    <div class="todo-rosa card-header">
                        <div class="card-title">Información General</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_usuario">Usuario<span class="text-danger"> *</span></label>
                                    <input id="id_usuario" type="text" class="form-control txtRosa" name="usuario" value="<?= set_value('usuario',@$usuario) ?>" placeholder="Usuario">
                                    <span class="form-text etiqueta-helper">Usuario para acceder al sistema</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_contrasena">Contraseña<span class="text-danger"> *</span></label>
                                    <input id="id_contrasena" type="password" class="form-control txtRosa" name="contrasena" value="<?= set_value('contrasena',@$contrasena) ?>" placeholder="Contraseña">
                                    <span class="form-text etiqueta-helper">Contraseña de acceso</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_ccontrasena">Confirmar Contraseña<span class="text-danger"> *</span></label>
                                    <input id="id_ccontrasena" type="password" class="form-control txtRosa" name="ccontrasena" value="<?= set_value('cccontrasena',@$cccontrasena) ?>" placeholder="Confirmar Contraseña">
                                    <span class="form-text etiqueta-helper">Confirmar contraseña</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_rol">Rol<span class="text-danger"> *</span></label>
                                    <?= form_dropdown('idRol',@$listaRoles,set_value('idrol',@$idRol),['class'=>'select2 form-control', 'id'=>'id_rol']) ?>
                                    <span class="form-text etiqueta-helper">Privilegios del usuario</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_nombre">Nombre<span class="text-danger"> *</span></label>
                                    <input id="id_nombre" type="text" class="form-control txtRosa" name="nombre" value="<?= set_value('nombre',@$nombre) ?>" placeholder="Nombre">
                                    <span class="form-text etiqueta-helper">Nombre del usuario</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_correo">Correo<span class="text-danger"> *</span></label>
                                    <input id="id_correo" type="text" class="form-control txtRosa" name="correo" value="<?= set_value('correo',@$correo) ?>" placeholder="Correo Electrónico">
                                    <span class="form-text etiqueta-helper">Indica el correo electrónico</span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label style="font-size: 1em!important" for="id_activo">Usuario Activo</label> <br>
                                    <label class="switch">
                                        <input type="checkbox" id="id_activo"  name="activo" <?=@$activo?> >
                                        <span></span>
                                    </label>
                                    <span class="form-text etiqueta-helper">Estado actual del usuario</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
        <div style="margin-top:20px; "class="col-12 m-t-3">
            <center>
                <button type="submit" class="btn todo-rosa">
                    <span class="fas fa-save"></span> Guardar
                </button>
            </center>
        </div>
    </div>
</div>
</div>
<?= form_close() ?>

