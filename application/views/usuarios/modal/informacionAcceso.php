<div class="row">
    <div class="col-sm-12">
        <h2 class="margin-bottom-5 text-dark-custom"><strong>Información De <?= $infoAcceso->nombre ?></strong></h2>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <div class="row text-left">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="id_creacion" class="margin-0">Creación</label><br>
                    <p class="text-danger margin-0 font-size-13"><?= (empty($infoAcceso->creacion))?"(Ninguno)":$infoAcceso->creacion ?></p>
                    <span class="form-text margin-0">Fecha de creación del usuario</span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="id_ultimo_acceso" class="margin-0">Fecha de Acción</label><br>
                    <p class="text-danger margin-0 font-size-13"><?= (empty($infoAcceso->concurrencia))?"(Ninguno)":$infoAcceso->concurrencia ?></p>
                    <span class="form-text margin-0">Fecha y hora de la última acción</span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="id_ultimo_acceso" class="margin-0">Último Módulo</label><br>
                    <p class="text-danger margin-0 font-size-13"><?= (empty($infoAcceso->alias))?"(Ninguno)":$infoAcceso->alias ?></p>
                    <span class="form-text margin-0">Último módulo visitado</span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="id_ultimo_acceso" class="margin-0">Última Acción</label><br>
                    <p class="text-danger margin-0 font-size-13"><?= (empty($infoAcceso->accion))?"(Ninguno)":$infoAcceso->accion ?></p>
                    <span class="form-text margin-0">Última acción realizada</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="text-center form-group">
            <label class="margin-0">Último Acceso</label><br>
            <p class="text-danger font-size-13"><?= (empty($infoAcceso->ultimoacc))?"(Ninguno)":$infoAcceso->ultimoacc ?></p>
            <div style="margin-top:51px">
                <?php if($infoAcceso->plataforma == 0): ?>
                    <span class="fas fa-unlink fa-4x"></span>
                <?php elseif($infoAcceso->plataforma == 1): ?>
                    <span class="fas fa-globe-americas fa-4x"></span>
                <?php elseif($infoAcceso->plataforma == 2): ?>
                    <span class="fas fa-mobile-alt fa-4x"></span>
                <?php elseif($infoAcceso->plataforma == 3): ?>
                    <span class="fas fa-plug fa-4x"></span>
                <?php else: ?>
                    <span class="fas fa-power-off fa-4x"></span>
                <?php endif; ?>
                <span class="form-text">Plataforma de acceso</span>
            <div>
        </div>
    </div>
</div>