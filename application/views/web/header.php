<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?= isset(getSistema()->titulo) ? getSistema()->titulo : 'gBase'?></title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <?php foreach ($this->config->item('plugins')['generales']['css'] as $plugin) echo $plugin; ?>

        <?php if ($estaLogueado)
                foreach ($this->config->item('plugins')['modulos'][getSistema()->modulo->nombre]['css'] as $plugin) echo $plugin; 
        ?>

    </head>

    <body class="fixed-left">
        
        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <div id="spinnerContainer" class="spinner_container">
            <div class="spinner2_background">
            </div>

            <div class="spinner2">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">
        <div class="messages-div">
        <div id="validacion">
        <?= (isset(getSistema()->mensajes) && getSistema()->mensajes != "validacion") ? getSistema()->mensajes : ''?>
        <?php if(validation_errors()): ?>
            <div class='alert alert-danger alert-dismissible fade show shadow'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                <h4 class='alert-heading'><span class='fas fa-bell'></span>Ha ocurrido un error</h4>
                <?=validation_errors() ?>
            </div>
        <?php endif; ?>
        </div>
</div>
<div class="backBlackInside"></div>
<div id="blackInside2" style="display:none" class="backBlackInside2"></div>
