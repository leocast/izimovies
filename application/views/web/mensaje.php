<div class='alert alert-<?=$tipo?> alert-dismissible fade show shadow'>
    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>×</span>
    </button>
<h5 class='alert-heading'>
    <span class='<?=$icono?>'></span> <?=$titulo?>
</h5>
<p><?=$texto?></p>
</div>