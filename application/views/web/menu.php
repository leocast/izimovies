            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="">
                        <a href="index.html" class="logo text-center">Base</a>
                        <!-- <a href="index.html" class="logo"><img src="assets/images/logo-sm.png" height="36" alt="logo"></a> -->
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">
                    <div style="border-radius: 15px;" id="sidebar-menu">
                        <ul>
                            <li style="padding-top: 40px!important;
                            font-size: 1.2em;
                            color: white;"class="menu-title">Menú</li>
                            <hr>

                            <?php foreach ($menu as $row): ?>
                            <li class="<?= (key_exists('submenu',$row))?'has_sub':'cargarLoader' ?>" >
                                <a style="color:white;" href="<?= base_url($row['nombre']) ?>" class="waves-effect">
                                    <i class="<?= $row['icono'] ?>"></i>
                                    <span><?= $row['alias'] ?>
                                    <?php if(key_exists('submenu',$row)):  ?>
                                        <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span>
                                    <?php endif; ?>
                                    </span>
                                </a>
                                <?php if(key_exists('submenu',$row)): ?>
                                    <ul class="list-unstyled">
                                        <?php foreach ($row['submenu'] as $sub): ?>
                                            <li class="<?= $this->uri->segment(1) == $sub['nombre'] ? "active" : ""?>">
                                                <a style="color:white;" href="<?= base_url($sub['nombre']) ?>" class="waves-effect cargarLoader">
                                                    <i class="<?= $sub['icono'] ?>"></i>
                                                    <span class="cargarLoader"><?= $sub['alias'] ?> </span>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                            <?php endforeach; ?>
<!--                     
                            <li class="menu-title">Prueba de notificacion</li>

                            <li>
                                <a href="contact.html" class="waves-effect"><i class="fa-message"></i><span> Mensajes <span class="badge badge-pill badge-danger pull-right">3</span> </span></a>
                            </li>  -->

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->


            <!-- Start right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <!-- style="color:white; background-color: #CE2E48 " -->
                    <div  class="topbar">

                        <nav style="background-color: #D34159; color:white;"class="navbar-custom">
                            <?php if(getSistema()->modulo->alias != "Inicio"): ?>
                            <!-- Search input -->
                            <div class="search-wrap" id="search-wrap">
                                <div class="search-bar">
                                <form action="">
                                    <input class="search-input" name="buscar" type="search" placeholder="Buscar" />
                                    <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                                    <i style="color:white!important" class="fas fa-times-circle"></i>
                                    </a>
                                </form>
                                </div>
                            </div>
                            <?php endif; ?>
                            <ul class="list-inline float-right mb-0">
                                <?php if(getSistema()->modulo->alias != "Inicio" && empty($this->uri->segment(3))): ?>
                                <!-- Search -->
                                <li class="list-inline-item dropdown notification-list">
                                    <a style="margin-bottom: -2.5px;" class="nav-link waves-effect toggle-search" href="#"  data-target="#search-wrap">
                                        <i style="color:white!important"class="menuColor fas fa-search"></i>
                                    </a>
                                </li>
                                <?php endif; ?>
                                <!-- Fullscreen -->
                                <!-- <li class="list-inline-item dropdown notification-list hidden-xs-down">
                                    <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                                        <i class="mdi mdi-fullscreen noti-icon"></i>
                                    </a>
                                </li>  -->
                                <!-- notification-->
                                <li  class="list-inline-item dropdown notification-list">
                                    <a style="color:#fff; margin-bottom: -2.5px;" class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                       <i style="color:white!important" class="menuColor fas fa-bell"></i>
                                        <span style="" class="badge badge-danger noti-icon-badge">3</span>
                                    </a>
                                    <div style="color:white!important; border: none; background-color: #CE2E48" 
                                    
                                    class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                        <div style="color:white!important;" class="dropdown-item noti-title">
                                            <h5>Notificaciones (3)</h5>
                                        </div>

                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="fas fa-shopping-cart"></i></div>
                                            <p style="color:white!important;" class="notify-details"><b>Orden enviada</b><small style="color:white!important;"  class="text-muted">Que buen texto de prueba para la orden.</small></p>
                                        </a>

                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-warning"><i class="fas fa-envelope"></i></div>
                                            <p style="color:white!important;" class="notify-details"><b>Nuevos mensajes</b><small  style="color:white!important;" class="text-muted">Tienes 3 mensajes sin leer</small></p>
                                        </a>
                                        <a style="color:white!important;" href="javascript:void(0);" class="dropdown-item notify-item">
                                            Ver todas las notificaciones
                                        </a>

                                    </div>
                                </li>
                                <!-- User-->
                                <li class="list-inline-item dropdown notification-list">
                                    <a style="color:#ffff;" class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <img src="<?=getSistema()->urlImagenPerfil?>" alt="user" class="rounded-circle">
                                    </a>
                                    <div style="color:white!important; border: none; background-color: #CE2E48"  class="dropdown-menu dropdown-menu-right profile-dropdown" style="min-width: 12rem!important;">
                                        <a style="color:white!important;"  class="dropdown-item" href="#"><i class="fas fa-user"></i></i> <?= getSistema()->usuarioNombre; ?></a>
                                        <!-- <div class="dropdown-divider"></div> -->
                                        <!-- <a class="dropdown-item" id="btn-fullscreen" href="#"><i class="dripicons-device-desktop text-muted"></i> Pantalla completa</a>
                                        <div class="dropdown-divider"></div> -->
                                        <a style="color:white!important;"  class="dropdown-item" href="<?=base_url()?>sesion/cerrarsesion"><i class="fas fa-sign-out-alt"></i> Salir</a>
                                    </div>
                                </li>
                            </ul>

                            <!-- Page title -->
                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button style="background-color: #D34159; color:white;" type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <div id="prueba">
                                    </div>
                                    <h3 class="page-title"><?=getSistema()->encabezado?></h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>
                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <!-- ==================
                         PAGE CONTENT START
                         ================== -->

                         <div class="page-content-wrapper">

                            <div class="container-fluid">