const Modulo = {
    configuracion:{
        baseUrl: "http://" + window.location.host + "/tortilleria/",
        loading_class: "progress-bar-striped progress-bar-animated",
        modulo: "",
        swal_custom: swal.mixin({
            confirmButtonClass: 'btn btn-primary margin-right-10',
            cancelButtonClass: 'btn btn-secondary margin-left-10',
            buttonsStyling: false })
    },
    funciones:{
        accion:function(accion){
            Modulo.loader.fadeIn();
            location.href = Modulo.configuracion.baseUrl+Modulo.configuracion.modulo+"/"+accion;
        },
        eliminar:function(e, id){
            e.preventDefault();
            // let swal_custom = Modulo.configuracion.swal_custom;
            Swal.fire({
                title: '¿Eliminar?',
                text: "Esta acción es irreversible",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    location.href = Modulo.configuracion.baseUrl+Modulo.configuracion.modulo+"/eliminar/"+id;
                } else if (result.dismiss === swal.DismissReason.cancel) {
                }
            });
        },
        mostrarMensaje: function(titulo, texto, tipo){
            var swal_custom = Modulo.configuracion.swal_custom;
            swal_custom.fire({
                title: titulo,
                text: texto,
                type: tipo,
                showCancelButton: false,
                confirmButtonText: 'Aceptar',
                confirmButtonClass: 'btn btn-primary',
                buttonsStyling: false
            }).then((result) => { });
        },
        mostrarModal: function(html, width = "500px"){
            var swal_custom = Modulo.configuracion.swal_custom;
            swal_custom.fire({
                html: html,
                width: width,
                showCancelButton: false,
                showConfirmButton: false,
            }).then((result) => { });
        },
        debugPhpError: function(html){
            if (html.includes("A PHP Error was encountered")) {

                html = html.split("<p>");

                let mensaje     = "<p>" + html[2].replace("Message:", "<strong>Mensaje:</strong>");
                let archivo     = "<p>" + html[3].replace("Filename:", "<strong>Nombre del archivo:</strong>");
                let numeroLinea = "<p>" + html[4].replace("Line Number:", "<strong>Número de línea:</strong>");
                let imagen      = "<img src='assets/img/otros/phplogo.png' width='150'>"
                let nuevoHtml = `
                    <br>
                    ${imagen}
                    <br><br>
                    <div style='text-align: start;'>
                        ${mensaje}
                        ${archivo}
                        ${numeroLinea}
                    </div>
                `;

                Swal.fire({
                    html: nuevoHtml,
                    width: "500px",
                    showConfirmButton: false,
                }).then((result) => { });

                return true;
            }

            return false;
              
        },
        navigation_detect_auto: function(){
            var path        = window.location.pathname,
                origin       = window.location.origin,
                pathArray   = path.split("/");
                
            $(".side-menu a[href='"+origin+"/"+pathArray[1]+"/"+pathArray[2]+"']").parent("li").addClass("active").parents(".has_sub").addClass("nav-active");
        },
        cambiarColorLabel: function(objeto, idLabel, idInput, textoTrue, textoFalse){
            
            if (objeto.length > 0) {
                $(`#${idLabel}`).removeClass("text-danger");
                $(`#${idLabel}`).html(textoTrue);
                $(`#${idInput}`).removeAttr('disabled');
            } else {
                $(`#${idLabel}`).addClass("text-danger");
                $(`#${idLabel}`).html(textoFalse);
                $(`#${idInput}`).attr('disabled', true);
            }
        },
        loaderInput: function(idInput, idLoader, type){

            switch (type) {
                case "show":
                    $(`#${idInput}`).attr('disabled', true);
                    $(`#${idLoader}`).slideDown("fast");
                    break;
            
                case "hide":
                    $(`#${idLoader}`).slideUp("fast");
                    $(`#${idInput}`).removeAttr('disabled');
                    break;
            }

        }
    },
    select2: function (){
        $(".select2").select2();
    },
    loader: {
        fadeIn: function (){
            $('#status').fadeIn();
            $('#preloader').delay(350).fadeIn('fast');
        },
        fadeOut: function (){
            $('#status').fadeOut();
            $('#preloader').delay(350).fadeOut('fast');
        }
    },
    wait:{
        show: function (){
            $('#spinnerContainer').show();
        },
        hide: function (){
            $('#spinnerContainer').hide();
        }
    },
    init:function () {

        window.addEventListener('beforeunload', function(event) {
            Modulo.loader.fadeIn();
        });

        Modulo.funciones.navigation_detect_auto();

    }
};

Modulo.init();