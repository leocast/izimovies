let Peliculas = {};

Peliculas.Build = (function () {
    
    function __construct(){
        Modulo.configuracion.modulo = "peliculas";
    };


    function Start() {  
        
    }

    return {
        Initialize: () => {
            __construct();
            Start();
        },
        public: {
           
        }
    }
});

$(document).ready(Peliculas.Build().Initialize());