let RegistrarPeliculas = {};

RegistrarPeliculas.Build = (function () {
    
    function __construct(){
        Modulo.configuracion.modulo = "registrarpeliculas";
    };


    function Start() {  
        
    }

    return {
        Initialize: () => {
            __construct();
            Start();
            
            $('.select2').select2();
        },
        public: {
           
        }
    }
});

$(document).ready(RegistrarPeliculas.Build().Initialize());