let Sesion = {};

Sesion.Build = (function () {
    
    function __construct(){

    };

    function verificarValidacion(){
        console.log($("#nuevoUsuario")[0]);

        
        if($("#nuevoUsuario")[0] == undefined){
            console.log("Soy null")
        }else{
            let nombre = $("#nuevoUsuario")[0].dataset.nombre;

            swal.fire({
                title: ``,
                html: `
                <h3>¡Gracias por registrarte <b>${nombre}</b>!</h3><br>`,
                type: 'success',
                width: "750px",
                confirmButtonText:
                '<i class="fa fa-thumbs-up"></i> Genial!',
                customClass: {
                    confirmButton: 'btn todo-rosa',
                  },
                onOpen: ()=>{
                    let button = $(".swal2-confirm");

                    button.addClass('todo-rosa');
                }
            })
        }

       
    }

    function Start() {  
        verificarValidacion();
    }

    return {
        Initialize: () => {
            __construct();
            Start();
        },
        public: {
           
        }
    }
});

$(document).ready(Sesion.Build().Initialize());