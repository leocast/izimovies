let Usuarios = {};

Usuarios.Build = (function () {
    
    function __construct(){
        Modulo.configuracion.modulo = "usuarios";
        $(".select2").select2();
    };

    function inicializarCheckbox(){
        $(':input[type=checkbox]').click(function () {
            var id = $(this).attr('id');
    
            if(id === "id_activo") return;
    
            var array = id.split('-');
            var action = array[2];
            var module = array[1];
    
            if(!$(this).is(':checked') && action === 'ACCE'){
                $("[id*=accion_ident-"+module+"]").each(function () {
                 $(this)[0].checked = "";
                })
            }else if($(this).is(':checked') && action !== 'ACCE' && !$("#accion_ident-"+module+"-ACCE").is(':checked')){
                $("#accion_ident-"+module+"-ACCE")[0].checked = 'checked';
            }
        });
    }

    function verImagen (){
        $('#urlImagen').change(function(e){
            var reader = new FileReader();
            reader.onload = function() {
                $('#id_perfil_image').attr("src",reader.result);
            };
            reader.readAsDataURL(e.target.files[0]);
        })
    }

    function getInformacionAcceso(idUsuario) {
        $.ajax({
            url : location.origin + '/izimovies/ajax/usuarios/informacionAcceso',
            type: 'POST',
            data: {
                "idUsuario" : idUsuario,
            },
            success : function(response) {
                try{
                    var res = JSON.parse(response);
                    if(res.estado === true){
                        Modulo.funciones.mostrarModal(res.result.html, "800px")
                    }else{
                        Modulo.funciones.mostrarMensaje("Advertencia",res.mensaje,"warning");
                    }
                } catch (e) {
                    Modulo.funciones.mostrarMensaje("ERROR","Error al gestionar la información","error");
                }
            }
        }).fail(function (e) {
            Modulo.funciones.mostrarMensaje("Error","Error al gestionar la información","error");
        });
    }

    //Función primaria
    function Start() {  
        inicializarCheckbox();
        verImagen();
    }

    return {
        Initialize: () => {
            __construct();
            Start();
        },
        public: {
            infoAcceso: (idUsuario) => {
                getInformacionAcceso(idUsuario);
            },
        }
    }
});

$(document).ready(Usuarios.Build().Initialize());