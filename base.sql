
DROP TABLE IF EXISTS `acciones`;
CREATE TABLE `acciones` (
  `id` varchar(32) NOT NULL,
  `idmodulo` varchar(32) DEFAULT NULL,
  `accion` varchar(128) DEFAULT NULL,
  `identificador` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "acciones"
#

INSERT INTO `acciones` VALUES ('1118e1b91f6269372fb8e1743d5d3518','4295e8065f2f640103f566df3329e17f','Acceso','ACCE'),('3a393ddfcb91efcf0f575da2be8a6cdc','4295e8065f2f640103f566df3329e17f','Eliminar','ELIM'),('5457ed48a82a9beeeab5b13473700e55','03f996214fba4a1d05a68b18fece8e71','Eliminar','ELIM'),('5573ddeeb0817bd291a4ff6759ffb013','03f996214fba4a1d05a68b18fece8e71','Modificar','MODI'),('5a72e728bf321bb09547d0e694a57cb5','03f996214fba4a1d05a68b18fece8e71','Acceso','ACCE'),('5ceee7ab594acb461cb8671e139937a9','893ce80729195afed54dd71286ef15bc','Acceso','ACCE'),('7a45e1491070e76d1f42c4ed19721422','4295e8065f2f640103f566df3329e17f','Modificar','MODI'),('a21dd88762c9fb79cb52bbf41fa3ad0a','0b382923b48a141421a9c559fa981ff5','Acceso','ACCE'),('d2a133c444de86065410f5ef9fb3ef3e','03f996214fba4a1d05a68b18fece8e71','Crear','CREA'),('e941762576cfa65657ba3a420a4d9284','4295e8065f2f640103f566df3329e17f','Crear','CREA'),('5','3','Acceso','ACCE'),('6','3','Crear','CREA'),('7','3','Modificar','MODI'),('8','3','Eliminar','ELIM'),('9','93614e93d9a874e206751752b7dee413','Acceso','ACCE'),('10','93614e93d9a874e206751752b7dee413','Crear','CREA'),('11','93614e93d9a874e206751752b7dee413','Modificar','MODI'),('12','93614e93d9a874e206751752b7dee413','Eliminar','ELIM'),('13','654614c2217c781b4f173302586ef981','Acceso','ACCE'),('14','654614c2217c781b4f173302586ef981','Crear','CREA'),('15','654614c2217c781b4f173302586ef981','Modificar','MODI'),('16','654614c2217c781b4f173302586ef981','Eliminar','ELIM'),('17','98ee4be6f5a177bbdce996fca042f845','Acceso','ACCE');

#
# Structure for table "modulos"
#

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE `modulos` (
  `id` varchar(32) NOT NULL,
  `nombre` varchar(32) DEFAULT NULL,
  `alias` varchar(64) DEFAULT NULL,
  `icono` varchar(32) DEFAULT NULL,
  `idmodulopad` varchar(32) DEFAULT NULL,
  `modulo` bit(1) DEFAULT NULL,
  `menu` bit(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `web` bit(1) DEFAULT NULL,
  `app` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "modulos"
#

INSERT INTO `modulos` VALUES ('03f996214fba4a1d05a68b18fece8e71','usuarios','Usuarios','fas fa-user','f68d60ad0137b5c2f0ad1fa5fe089eba',b'1',b'1',9,b'1',b'0'),('0b382923b48a141421a9c559fa981ff5','reportes','Reportes','fas fa-chart-line',NULL,b'1',b'1',10,b'1',b'0'),('4295e8065f2f640103f566df3329e17f','roles','Roles','fas fa-key','f68d60ad0137b5c2f0ad1fa5fe089eba',b'1',b'1',8,b'1',b'0'),('893ce80729195afed54dd71286ef15bc','inicio','Inicio','fas fa-home',NULL,b'1',b'1',1,b'1',b'0'),('ac46ab58f501d658b7163cdca12ffb32','m_catalogos','Catálogos','fas fa-tags',NULL,b'1',b'0',5,b'1',b'0'),('f68d60ad0137b5c2f0ad1fa5fe089eba','m_configuracion','Configuración','fas fa-cogs',NULL,b'1',b'0',7,b'1',b'0'),('93614e93d9a874e206751752b7dee413','categorias','Categorías','fas fa-book','ac46ab58f501d658b7163cdca12ffb32',b'1',b'1',3,b'1',b'0'),('654614c2217c781b4f173302586ef981','registrarpeliculas','Películas','fas fa-film','ac46ab58f501d658b7163cdca12ffb32',b'1',b'1',4,b'1',b'0'),('98ee4be6f5a177bbdce996fca042f845','peliculas','Películas','fas fa-video',NULL,b'1',b'1',2,b'1',b'0');

#
# Structure for table "permisos"
#

DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idaccion` varchar(32) NOT NULL DEFAULT '0',
  `idrol` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

#
# Data for table "permisos"
#

INSERT INTO `permisos` VALUES (85,'5ceee7ab594acb461cb8671e139937a9','2a2e9a58102784ca18e2605a4e727b5f'),(86,'17','2a2e9a58102784ca18e2605a4e727b5f'),(87,'9','2a2e9a58102784ca18e2605a4e727b5f'),(88,'10','2a2e9a58102784ca18e2605a4e727b5f'),(89,'12','2a2e9a58102784ca18e2605a4e727b5f'),(90,'11','2a2e9a58102784ca18e2605a4e727b5f'),(91,'13','2a2e9a58102784ca18e2605a4e727b5f'),(92,'14','2a2e9a58102784ca18e2605a4e727b5f'),(93,'16','2a2e9a58102784ca18e2605a4e727b5f'),(94,'15','2a2e9a58102784ca18e2605a4e727b5f'),(95,'1118e1b91f6269372fb8e1743d5d3518','2a2e9a58102784ca18e2605a4e727b5f'),(96,'e941762576cfa65657ba3a420a4d9284','2a2e9a58102784ca18e2605a4e727b5f'),(97,'3a393ddfcb91efcf0f575da2be8a6cdc','2a2e9a58102784ca18e2605a4e727b5f'),(98,'7a45e1491070e76d1f42c4ed19721422','2a2e9a58102784ca18e2605a4e727b5f'),(99,'5a72e728bf321bb09547d0e694a57cb5','2a2e9a58102784ca18e2605a4e727b5f'),(100,'d2a133c444de86065410f5ef9fb3ef3e','2a2e9a58102784ca18e2605a4e727b5f'),(101,'5457ed48a82a9beeeab5b13473700e55','2a2e9a58102784ca18e2605a4e727b5f'),(102,'5573ddeeb0817bd291a4ff6759ffb013','2a2e9a58102784ca18e2605a4e727b5f'),(103,'a21dd88762c9fb79cb52bbf41fa3ad0a','2a2e9a58102784ca18e2605a4e727b5f'),(104,'5ceee7ab594acb461cb8671e139937a9','bfa5d96b849eb8c080877e397d9b46d2'),(105,'17','bfa5d96b849eb8c080877e397d9b46d2');

#
# Structure for table "roles"
#

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `nombre` varchar(50) DEFAULT NULL,
  `creacion` datetime DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "roles"
#

INSERT INTO `roles` VALUES ('2a2e9a58102784ca18e2605a4e727b5f','Administrador','2019-05-14 00:00:00',b'1'),('bfa5d96b849eb8c080877e397d9b46d2','Cliente','2019-09-20 22:44:12',b'1');

#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` varchar(32) NOT NULL,
  `idrol` varchar(32) DEFAULT NULL,
  `nombre` varchar(64) DEFAULT NULL,
  `usuario` varchar(16) DEFAULT NULL,
  `contrasena` varchar(32) DEFAULT NULL,
  `tokenaut` varchar(32) DEFAULT NULL,
  `tokenrec` varchar(32) DEFAULT NULL,
  `tokenapi` varchar(32) DEFAULT NULL,
  `ultimoacc` datetime DEFAULT NULL,
  `imagen` varchar(128) DEFAULT NULL,
  `correo` varchar(128) DEFAULT NULL,
  `plataforma` int(11) DEFAULT NULL,
  `idultimomod` varchar(32) DEFAULT NULL,
  `idultimaacc` varchar(32) DEFAULT NULL,
  `ultimorec` varchar(32) DEFAULT NULL,
  `concurrencia` datetime DEFAULT NULL,
  `creacion` datetime DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "usuarios"
#

INSERT INTO `usuarios` VALUES ('2809d4d68d087905ac9ac1f6b9d8cfdf','2a2e9a58102784ca18e2605a4e727b5f','Leo Castellanos','lcas','e10adc3949ba59abbe56e057f20f883e','97d6e56ea57784ee327f700c7abb056d','21816d1aeccf15b5dcb3c13582d34eee','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1','2019-09-22 23:49:09','default.jpg','leoocast@gmail.com',4,NULL,NULL,NULL,'2019-09-22 23:50:38','2019-08-10 00:00:00',b'1'),('2a2e9a58102784ca18e2605a4e727b5f','2a2e9a58102784ca18e2605a4e727b5f','Administrador','admin','e10adc3949ba59abbe56e057f20f883e','62742eca612dfedcedaf5ebbe5c13c3a',NULL,NULL,'2019-05-14 09:25:26','default.jpg','admin@proyectofinal.com',4,NULL,NULL,NULL,'2019-05-14 09:26:30','2019-08-10 00:00:00',b'1'),('2e9bdcb09c0a2cc6ea2a929285459e2e','2a2e9a58102784ca18e2605a4e727b5f','Cézar Urias','yuko','e10adc3949ba59abbe56e057f20f883e','9e8440705f25e023887178f80fb08a77',NULL,NULL,'2019-09-22 15:44:47','default.jpg','emailprueba@gmail.com',1,NULL,'9',NULL,'2019-09-22 15:46:13','2019-09-22 12:44:00',b'1'),('1fe2cc4876df2e19c7973fe766d23fc5','bfa5d96b849eb8c080877e397d9b46d2','Suker','suker','e10adc3949ba59abbe56e057f20f883e','5569c932e8d6bd9c4cbdc8979a56547b',NULL,NULL,'2019-09-22 23:37:43','1fe2cc4876df2e19c7973fe766d23fc5.jpg','suker@suker.com',4,NULL,NULL,NULL,'2019-09-22 23:44:33','2019-09-22 22:04:16',b'1'),('510fd6ba7c0fda61903f07387832830b','bfa5d96b849eb8c080877e397d9b46d2','Eva','eva','e10adc3949ba59abbe56e057f20f883e','4e3ee377d4db57577def249d900f4eed',NULL,NULL,'2019-09-22 23:47:50','perfilRegistro.jpg','eva@gmail.com',4,NULL,NULL,NULL,'2019-09-22 23:49:04','2019-09-22 23:47:41',b'1');
